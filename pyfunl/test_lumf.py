import coscalc
flrw1 = coscalc.Flrw(71,0.27,0.73)
from math import log10,log    
import lumfuncs

h = 0.71 # cosmology
phi_star = 1.6e-2*h**3
M_star = -19.7+5*log10(h)
alpha = -1.07
M_solar = 4.74
L_solar = 3.856e33
L_star = h**-2*10**(0.4*(M_solar-M_star)) * L_solar
lf1 = lumfuncs.Schechter(phi_star,L_star,alpha)
lf1.object_density(1e40,L_star)
lf1.number_objects(flrw1,0,1,1e40,L_star)
Ni, vol=lf1.object_number(flrw1,0,1,1e40,L_star)

import astro_unit
astro_unit.str2arcsec2(lf1.number_objects(flrw1,0,1,1e40,L_star))

#------------------------------------------------

import compatibility.metrics as metrics
flrw2 = metrics.Flrw(71,0.27,0.73)
from math import log10,log    
import compatibility.lumfuncs as lumfuncs

h = 0.71 # cosmology
phi_star = 1.6e-2*h**3
M_star = -19.7+5*log10(h)
alpha = -1.07
M_solar = 4.74
L_solar = 3.856e33
L_star = h**-2*10**(0.4*(M_solar-M_star)) * L_solar
lf2 = lumfuncs.Schechter(phi_star,L_star,alpha)
lf2.object_density(1e40,L_star)
lf2.number_objects(flrw2,0,1,1e40,L_star)


import astro_unit
astro_unit.str2arcsec2(lf2.number_objects(flrw2,0,1,1e40,L_star))


#-------------------------------------------------
from math import log10,log  
import milia.lumfuncs as lumfuncs

M_solar = 4.74
L_solar = 3.856e33
phi_star0 = 1.62e-3
g_phi = -6.15
M_star0 = -20.7
L_star0 = 10**(0.4*(M_solar-M_star0)) * L_solar
g_l = -1.77
alph0 = -1.05
g_a = 1.31
lf = lumfuncs.Schechter(phi_star0, g_phi, L_star0, g_l, alph0, g_a, 0)
#tmp = lf.function(L_star0) #1.0305842651368782e-47
print lf
lf.evolve(0.01)
#tmp = lf.function(L_star0) #4.4416964234468374e-51
print lf


#schechter(phi*=0.00162, lum*=5.78278e+43, alpha=-1.05, z=0)
#schechter(phi*=0.00152384, lum*=5.68183e+43, alpha=-1.0369, z=0.01)

#-------------------------------------------

from math import log10,log    
import lumfuncs

M_solar = 4.74
L_solar = 3.856e33
phi_star0 = 1.62e-3
g_phi = -6.15
M_star0 = -20.7
L_star0 = 10**(0.4*(M_solar-M_star0)) * L_solar
g_l = -1.77
alph0 = -1.05
g_a = 1.31
lf = lumfuncs.Schechter(phi_star0, g_phi , L_star0, g_l, alph0, g_a, 0)
#tmp = lf.function(L_star0) #1.0305842651368782e-47
print lf
lf.test_()
#lf.evolve(0.01)
#tmp = lf.function(L_star0) #2.7223675903968802e-53
print lf



#schechter(phi*=0.00162, lum*=5.7827847257e+43, alpha=-1.05, z=0)
#schechter(phi*=0.00152383718032, lum*=5.68182945206e+43, alpha=-1.05, z=0.01)
