#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (c) 2009 Nicolas Gruel All rights reserved.
# This program or module is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, 
# or (at your option) any later version. It is distributed in the hope that
# it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
# the GNU General Public License for more details.

import sys
import os

from math import log10

from PyQt4.QtCore import *
from PyQt4.QtGui import *

import ui_pyfunl
import luminosityfunctiondlg

import astro_unit
#import filterdata

progname = os.path.basename(sys.argv[0])
progversion = "0.1"

__py_version__ = float(sys.version[:3]) #remplacer par sys.version_info
    
try:
    import compatibility.metrics as coscalc
except ImportError:
    import coscalc
    
try:
    import compatibility.lumfuncs as lumfuncs
except ImportError:
    import lumfuncs
    
MAC = True
try:
    from PyQt4.QtGui import qt_mac_set_native_menubar
except ImportError:
    MAC = False

flrw = coscalc.Flrw(71,0.27,0.73)

class PyFunl(QMainWindow, ui_pyfunl.Ui_Pyfunl):
    
    def __init__(self, parent=None):
        super(PyFunl, self).__init__(parent)
        
        self.setAttribute(Qt.WA_DeleteOnClose)

        self.file_menu = QMenu('&File', self)
        self.file_menu.addAction('&Quit', self.fileQuit,
                                 Qt.CTRL + Qt.Key_Q)
        self.menuBar().addMenu(self.file_menu)

        self.help_menu = QMenu('&Help', self)
        self.menuBar().addSeparator()
        self.menuBar().addMenu(self.help_menu)

        self.help_menu.addAction('&About', self.about)

        self.__H0 = flrw.get_hubble()
        self.__OmegaM = flrw.get_matter()
        self.__OmegaL = flrw.get_vacuum()
   
        self.setupUi(self)
        self.matterLabel.setText(u'\u03a9M')
        self.vacuumLabel.setText(u'\u03a9\u03bb')

        self.frame.setEnabled(False) 
                
        #self.unitComboBox.insertItem(0, '', [None,''])
        self.unitComboBox.addItems(astro_unit.LF_unit)

        self.lambdacLabel.setText(u'\u03bb(filter)')
        self.fwhmLabel.setText('FWHM(filter)')
        self.lambdalineLabel.setText(u'\u03bb(line)')

        self.row = 0
        self.LF_container = [] #TODO: need to be done properly as a container and yield
        
        self.initForm()
        
        if not MAC:
            self.buttonBox.setFocusPolicy(Qt.NoFocus)
        
        self.connect(self.hubbleEdit,SIGNAL('returnPressed()'), self.returnHubble)
        self.connect(self.matterEdit,SIGNAL('returnPressed()'), self.returnMatter)
        self.connect(self.vacuumEdit,SIGNAL('returnPressed()'), self.returnVacuum)
        
        self.connect(self.unitComboBox,SIGNAL("currentIndexChanged(int)"), self.updateUnitComboBox)

        self.connect(self.galaxypopPushButton,SIGNAL('clicked()'), self.editAddLF)
        self.connect(self.buttonBox.button(QDialogButtonBox.Reset), SIGNAL('clicked()'), self.resetForm)        
        self.connect(self.buttonBox.button(QDialogButtonBox.Apply), SIGNAL('clicked()'), self.apply)
        self.connect(self.buttonBox.button(QDialogButtonBox.Cancel), SIGNAL('clicked()'), SLOT('close()'))

    def validateHubble(self):
        '''Update Age in function of the cosmology'''
        
        try: 
            if self.hubbleEdit.text().isEmpty() or self.hubbleEdit.text() == ' ':
                raise ValueError, 'Please enter a Hubble parameter'                
            self.H0 = float(self.hubbleEdit.text())
            if self.H0 <= 0:
                raise ValueError, 'Hubble constant <= 0 not allowed'
            #to be compatible with milia
#            if hasattr(flrw, 'hubble'):
#                flrw.hubble = self.H0
#            else:
            flrw.set_hubble(self.H0)
            return True
        except ValueError, e:
            QMessageBox.warning(self,'Hubble constant error', unicode(e))
            self.hubbleEdit.setText('')
            self.hubbleEdit.selectAll()
            self.hubbleEdit.setFocus()
            return False
        
    def returnHubble(self):
        self.validateHubble()
        if self.hubbleEdit.text().isEmpty() or self.hubbleEdit.text() == ' ':
            self.hubbleEdit.setFocus()
        else:  
            self.matterEdit.setFocus()
    
    def validateMatter(self):
        '''Update Matter density in function of the cosmology given'''
        try: 
            if self.matterEdit.text().isEmpty() or self.matterEdit.text() == ' ':
                raise ValueError, 'Please enter a matter density value'                
            self.OmegaM = float(self.matterEdit.text())
            if self.OmegaM < 0:
                raise ValueError, 'Matter density < 0 not allowed'
            #to be compatible with milia
#            if hasattr(flrw, 'matter'):
#                flrw.matter = self.OmegaM
#            else:
            flrw.set_matter(self.OmegaM)
            return True        
        except ValueError, e:
            QMessageBox.warning(self,'Matter density error', unicode(e))
            self.matterEdit.setText('')
            self.matterEdit.selectAll()
            self.matterEdit.setFocus()
            return False

    def returnMatter(self):
        self.validateMatter()
        if self.matterEdit.text().isEmpty() or self.matterEdit.text() == ' ':
            self.matterEdit.setFocus()
        else:  
            self.vacuumEdit.setFocus()
                
    def validateVacuum(self):
        '''Update Vacuum density in function of the cosmology'''
        try:
            if self.vacuumEdit.text().isEmpty() or self.vacuumEdit.text() == ' ':
                print self.vacuumEdit.text().isEmpty(), 
                raise ValueError, 'Please enter a vacuum density value'                            
            self.OmegaL = float(self.vacuumEdit.text())
            if self.OmegaL < 0:
                raise ValueError, 'The Universe recolapse'
            #to be compatible with milia
#            if hasattr(flrw, 'vacuum'):
#                flrw.vacuum = self.OmegaL
#            else:
            flrw.set_vacuum(self.OmegaL)
            return True
        except ValueError, e:
            QMessageBox.warning(self,'Vacuum density error: ', unicode(e))
            self.vacuumEdit.selectAll()
            self.vacuumEdit.setFocus()
            return False

    def returnVacuum(self):
        self.validateVacuum()
        if self.vacuumEdit.text().isEmpty():
            self.vacuumEdit.setFocus()
        else:  
            self.skyareaEdit.setFocus()

#TODO:
    def validateSkyArea(self):
        '''Update redshift in function of the cosmology'''
        try:
            if self.skyareaEdit.text().isEmpty():
                raise ValueError, 'Please enter a sky area in arcsec^2' #TODO:
            self.skyarea = float(self.skyareaEdit.text())
            if self.skyarea < 0:
                raise ValueError, 'The sky area must be positif'
            return True
        except ValueError, e:
            QMessageBox.warning(self,'skyarea error', unicode(e))
            self.skyareaEdit.selectAll()
            self.skyareaEdit.setFocus()
            return False
        
    def returnSkyArea(self):
        self.validateSkyArea()
        if self.skyareaEdit.text().isEmpty():
            self.skyareaEdit.setFocus()
        else:  
            self.z_depEdit.setFocus()

#TODO: generalized as a function
       
    def validatez_dep(self):
        '''
        '''
        try:
            if self.z_depEdit.text().isEmpty():
                raise ValueError, 'Please enter a redshift'
            self.z_dep = float(self.z_depEdit.text())
            if self.z_dep < 0:
                raise ValueError, 'The redshift must be positif'
            return True
        except ValueError, e:
            QMessageBox.warning(self,'Redshift error', unicode(e))
            self.z_depEdit.selectAll()
            self.z_depEdit.setFocus()
            return False
        
    def returnz_dep(self):
        self.validatez_dep()
        if self.z_depEdit.text().isEmpty():
            self.z_depEdit.setFocus()
        else:  
            self.z_endEdit.setFocus()

    def validatez_end(self):
        '''
        '''
        try:
            if self.z_endEdit.text().isEmpty():
                raise ValueError, 'Please enter a redshift'
            self.z_end = float(self.z_endEdit.text())
            if self.z_end < 0:
                raise ValueError, 'The redshift must be positif'
            return True
        except ValueError, e:
            QMessageBox.warning(self,'Redshift error', unicode(e))
            self.z_endEdit.selectAll()
            self.z_endEdit.setFocus()
            return False
        
    def returnz_end(self):
        self.validatez_end()
        if self.z_endEdit.text().isEmpty():
            self.z_endEdit.setFocus()
        else:  
            self.maglimEdit.setFocus()

    def validateMagLim(self):
        '''
        '''
        try:
            if self.maglimEdit.text().isEmpty():
                raise ValueError, 'Please enter a magnitude'
            self.maglim = float(self.maglimEdit.text())
            return True
        except ValueError, e:
            QMessageBox.warning(self,'magnitude error', unicode(e))
            self.maglimEdit.selectAll()
            self.maglimEdit.setFocus()
            return False
        
    def returnMagLim(self):
        self.validateMagLim()
        if self.maglimEdit.text().isEmpty():
            self.maglimEdit.setFocus()
        else:  
            self.galaxypopPushButton.setFocus()

    def initForm(self):
        '''Reset the parameters at their default value '''
                
        self.H0 = float(self.__H0)
        flrw.set_hubble(self.H0)
        self.OmegaM = float(self.__OmegaM)
        flrw.set_matter(self.OmegaM)
        self.OmegaL = float(self.__OmegaL)
        flrw.set_vacuum(self.OmegaL)

        if __py_version__ < 2.6: 
            self.hubbleEdit.setText('%.2f ' % (self.__H0))
            self.matterEdit.setText('%.2f' % (self.__OmegaM))
            self.vacuumEdit.setText('%.2f' % (self.__OmegaL))
        else:
            self.hubbleEdit.setText('{0:.2f}'.format(self.__H0))    
            self.matterEdit.setText('{0:.2f}'.format(self.__OmegaM))
            self.vacuumEdit.setText('{0:.2f}'.format(self.__OmegaL))
 
        self.skyareaEdit.setText('')
        if hasattr(self,'skyarea'):
            self.skyarea = None
        self.z_depEdit.setText('')
        self.z_endEdit.setText('')
        if hasattr(self,'z_dep'):
            self.z_dep  = None  
        if hasattr(self,'z_end'):
            self.z_end = None
        self.maglimEdit.setText('')
        if hasattr(self,'maglim'):
            self.maglim = None

    def resetForm(self):
        '''Reset the parameters at their default value '''
                        
        self.H0 = None
        self.OmegaM = None
        self.OmegaL = None
        
        self.hubbleEdit.setText('')
        self.matterEdit.setText('')
        self.vacuumEdit.setText('')
        
        self.skyareaEdit.setText('')
        if hasattr(self,'skyarea'):
            self.skyarea = None
        self.z_depEdit.setText('')
        self.z_endEdit.setText('')
        if hasattr(self,'z_dep'):
            self.z_dep  = None  
        if hasattr(self,'z_end'):
            self.z_end = None
        self.maglimEdit.setText('')
        if hasattr(self,'maglim'):
            self.maglim = None

    def apply(self):
        '''Launch the calcul and verification'''
        #TODO: A finir changer la ou le resultat s'affiche! Mettre sous forme de table

        if not self.validateHubble():
            return        
        if not self.validateMatter():
            return
        if not self.validateVacuum():
            return
        if not self.validateMagLim():
            return
        if not self.validateSkyArea():
            return
        if not self.validatez_dep():
            return
        if not self.validatez_end():
            return
        #if not self.validateRedshift():
        #    return
        #self.checkbox[self.angularscaleCheckBox][0] = 'Angular scale : 1"'
        #self.updateAge()
        #if hasattr(self,'tableWidget'):
        #    self.updateTable()
        #self.updateTextEdit()
        if self.frame.isEnabled():
            self.redshiftFilter()

        #TODO: Add it in the documentation
        #   phi_star = phi_star0 * h^3
        #   M_star = M_star0 + 5log10(h)
        h = flrw.get_hubble()/100.
        self.phistar = self.LFdata.phistar*(h**3)
        self.Mstar = self.LFdata.Mstar+5*log10(h)
        self.Lstar = astro_unit.magabs2ergs(self.LFdata.Mstar,self.LFdata.filter_fwhm)
        self.alpha = self.LFdata.alpha
        
        #print self.phistar, self.Mstar, self.Lstar, self.alpha
        #TODO: Change extinction and kcorrection to be observational data not LFdata!!!
        #      Check where to apply this correction? on mag lim or Mstar or both?
        #      probably only maglim        
        if self.LFdata.extinction:
            self.maglim += self.LFdata.extinction
            
        if ((self.LFdata.ephistar is None) or (self.LFdata.eLstar is None) or 
            (self.LFdata.ealpha is None) or (self.LFdata.redshift_lim is None)):
            self.LF = lumfuncs.Schechter(self.phistar, self.Lstar, self.alpha)
        else:
            self.LF = lumfuncs.Schechter(self.phistar, self.LFdata.ephistar,
                                         self.Lstar, self.LFdata.eLstar,self.alpha,
                                         self.LFdata.ealpha, 0)
        
#        if self.LFdata.kcorr_filter is None:
#            if self.LFdata.redshift_lim is None:
#                N = self.LF.object_number(flrw, self.z_dep, self.z_end, self.maglim, 
#                                          self.LFdata.Mstar,self.LFdata.filter_fwhm)
#            else:
#                N = self.LF.object_number(flrw, self.z_dep, self.z_end, self.maglim, 
#                                          self.LFdata.Mstar,self.LFdata.filter_fwhm, 
#                                          zlim=self.LFdata.redshift_lim)
#        else:
#            if self.LFdata.redshift_lim is None:
#                N = self.LF.object_number(flrw, self.z_dep, self.z_end, self.maglim, 
#                                          self.LFdata.Mstar,self.LFdata.filter_fwhm, 
#                                          catalog=self.LFdata.kcorr_cat, filter=self.LFdata.kcorr_filter, 
#                                          galaxytype=self.LFdata.galaxytype)
#            else:
#                N = self.LF.object_number(flrw, self.z_dep, self.z_end, self.maglim, 
#                                          self.LFdata.Mstar,self.LFdata.filter_fwhm, 
#                                          catalog=self.LFdata.kcorr_cat, filter=self.LFdata.kcorr_filter, 
#                                          galaxytype=self.LFdata.galaxytype, zlim=self.LFdata.redshift_lim)
   
   
        N = self.LF.number_objects2(flrw, self.z_dep, self.z_end, self.maglim, 
                                          self.Mstar, self.LFdata.filter_fwhm)
        self.N = self.skyarea * N / astro_unit.conversion['str2arcsec2']

#        N = self.LF.object_number2(flrw, self.z_dep, self.z_end, self.maglim, 
#                                          self.Mstar, self.LFdata.filter_fwhm)
#        self.N = self.skyarea*sum(N) / astro_unit.conversion['str2arcsec2'] 
        
        #BUG: TODO: Degager la valeur absolue!!!! Il y a un gros bins la mais je ne sais pas pourquoi!!!
        #QMessageBox.information(self,'Pyfunl Result', unicode('Number of galaxies observable:\n'+str(abs(self.N))))
        QMessageBox.information(self,'Pyfunl Result', unicode('Number of galaxies observable:\n'+str(self.N)))
        

    def redshiftFilter(self):
        lc = float(self.lambdacLineEdit.text())
        fwhm = float(self.fwhmLineEdit.text()) 
        ll = float(self.lambdalineLineEdit.text())
        self.z_dep = ((lc-fwhm/2) / ll)-1
        self.z_end = ((lc+fwhm/2) / ll)-1
        self.z_depEdit.setText(str(self.z_dep))
        self.z_endEdit.setText(str(self.z_end))

    def fileQuit(self):
        self.close()

    def about(self):
        QMessageBox.about(self, "About %s" % progname,
u"""%(prog)s version %(version)s
Copyright \N{COPYRIGHT SIGN} 2009 Nicolas Gruel
"""
% {"prog": progname, "version": progversion})

    def editAddLF(self):
        form = luminosityfunctiondlg.LuminosityFunctionDlg()
        if form.exec_():
            self.LFdata = form.LFdata
            self.updateTable()
            
    def updateTable(self):
        r'''Update table view with characteristics of the LF
       
        TODO: For now only one LF available!  
        '''
        #TODO: ADD observational characteristics!!!!
        self.row += 1
        
        self.LF_container.append(self.LFdata.tableItems())
        self.tableWidget.clear()
        self.tableWidget.setColumnCount(len(self.LFdata.header))
        self.tableWidget.setRowCount(self.row)
        
        header = self.LFdata.header
#        header.append('Number of galaxies')
        self.tableWidget.setHorizontalHeaderLabels(header)
        
        for row in range(self.row):
            dohide = []
            hide = []
            for col in range(len(self.LF_container[row])):
                if (self.LF_container[row][col] is None):
                    hide.append(col)            
                if __py_version__ < 2.6:
                    item = QTableWidgetItem('%s' % (self.LF_container[row][col]))
                else:
                    item = QTableWidgetItem('{0}'.format(self.LF_container[row][col]))
                item.setTextAlignment(Qt.AlignRight|Qt.AlignVCenter)
                self.tableWidget.setItem(row,col,item)    
            
            if len(hide):
                dohide.append(row)

        if len(dohide) == self.row:
            self.colid = hide
            for col in hide:
                self.tableWidget.hideColumn(col)
        else:
            if self.colid:
                for col in self.colid:
                    self.tableWidget.showColumn(col)

        self.tableWidget.resizeColumnsToContents()
        
    def updateUnitComboBox(self):
        #Choisir vers quoi on transforme... => Luminosity (erg/s) pour LF mais pour kcorrection et extinction inverse. 
        pass
        #if self.unitComboBox.currentIndex():
        #    #TODO:  
        #    method,unit = self.metrics_method[unicode(self.unitComboBox.currentText())]
        
def main():
    '''Function to start the GUI. Needed by setuptools.
    '''
    app = QApplication(sys.argv)
    pyfunl = PyFunl()
    pyfunl.show()
    app.exec_()
    
if __name__ == '__main__':
    main()