# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file './luminosityfunctiondlg.ui'
#
# Created: Wed Dec 16 10:36:39 2009
#      by: PyQt4 UI code generator 4.6
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

class Ui_LuminosityFunctionDlg(object):
    def setupUi(self, LuminosityFunctionDlg):
        LuminosityFunctionDlg.setObjectName("LuminosityFunctionDlg")
        LuminosityFunctionDlg.resize(372, 507)
        self.gridLayout_3 = QtGui.QGridLayout(LuminosityFunctionDlg)
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.LFnameLabel = QtGui.QLabel(LuminosityFunctionDlg)
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setWeight(75)
        font.setBold(True)
        self.LFnameLabel.setFont(font)
        self.LFnameLabel.setObjectName("LFnameLabel")
        self.horizontalLayout_2.addWidget(self.LFnameLabel)
        self.LFnameLineedit = QtGui.QLineEdit(LuminosityFunctionDlg)
        self.LFnameLineedit.setObjectName("LFnameLineedit")
        self.horizontalLayout_2.addWidget(self.LFnameLineedit)
        self.gridLayout_3.addLayout(self.horizontalLayout_2, 0, 0, 1, 3)
        self.label_2 = QtGui.QLabel(LuminosityFunctionDlg)
        font = QtGui.QFont()
        font.setWeight(75)
        font.setBold(True)
        self.label_2.setFont(font)
        self.label_2.setObjectName("label_2")
        self.gridLayout_3.addWidget(self.label_2, 1, 0, 1, 2)
        self.label_4 = QtGui.QLabel(LuminosityFunctionDlg)
        self.label_4.setObjectName("label_4")
        self.gridLayout_3.addWidget(self.label_4, 2, 0, 1, 1)
        self.filterComboBox = QtGui.QComboBox(LuminosityFunctionDlg)
        self.filterComboBox.setObjectName("filterComboBox")
        self.gridLayout_3.addWidget(self.filterComboBox, 2, 1, 1, 1)
        self.horizontalLayout_5 = QtGui.QHBoxLayout()
        self.horizontalLayout_5.setObjectName("horizontalLayout_5")
        self.gridLayout = QtGui.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.phistarLabel = QtGui.QLabel(LuminosityFunctionDlg)
        self.phistarLabel.setObjectName("phistarLabel")
        self.gridLayout.addWidget(self.phistarLabel, 0, 0, 1, 1)
        self.phistarLineedit = QtGui.QLineEdit(LuminosityFunctionDlg)
        self.phistarLineedit.setObjectName("phistarLineedit")
        self.gridLayout.addWidget(self.phistarLineedit, 0, 1, 1, 1)
        self.LstarLabel = QtGui.QLabel(LuminosityFunctionDlg)
        self.LstarLabel.setObjectName("LstarLabel")
        self.gridLayout.addWidget(self.LstarLabel, 1, 0, 1, 1)
        self.LstarLineedit = QtGui.QLineEdit(LuminosityFunctionDlg)
        self.LstarLineedit.setObjectName("LstarLineedit")
        self.gridLayout.addWidget(self.LstarLineedit, 1, 1, 1, 1)
        self.unitComboBox = QtGui.QComboBox(LuminosityFunctionDlg)
        self.unitComboBox.setObjectName("unitComboBox")
        self.gridLayout.addWidget(self.unitComboBox, 1, 2, 1, 1)
        self.alphaLabel = QtGui.QLabel(LuminosityFunctionDlg)
        self.alphaLabel.setObjectName("alphaLabel")
        self.gridLayout.addWidget(self.alphaLabel, 2, 0, 1, 1)
        self.alphaLineedit = QtGui.QLineEdit(LuminosityFunctionDlg)
        self.alphaLineedit.setObjectName("alphaLineedit")
        self.gridLayout.addWidget(self.alphaLineedit, 2, 1, 1, 1)
        self.gridLayout.setColumnStretch(1, 1)
        self.gridLayout.setColumnStretch(2, 2)
        self.horizontalLayout_5.addLayout(self.gridLayout)
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_5.addItem(spacerItem)
        self.gridLayout_3.addLayout(self.horizontalLayout_5, 3, 0, 1, 3)
        self.horizontalLayout_4 = QtGui.QHBoxLayout()
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.verticalLayout_2 = QtGui.QVBoxLayout()
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.pushButton = QtGui.QPushButton(LuminosityFunctionDlg)
        self.pushButton.setCheckable(True)
        self.pushButton.setObjectName("pushButton")
        self.verticalLayout_2.addWidget(self.pushButton)
        self.horizontalLayout.addLayout(self.verticalLayout_2)
        spacerItem1 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem1)
        self.frame = QtGui.QFrame(LuminosityFunctionDlg)
        self.frame.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtGui.QFrame.Sunken)
        self.frame.setObjectName("frame")
        self.gridLayout_2 = QtGui.QGridLayout(self.frame)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.ephistarLabel = QtGui.QLabel(self.frame)
        self.ephistarLabel.setObjectName("ephistarLabel")
        self.gridLayout_2.addWidget(self.ephistarLabel, 0, 0, 1, 1)
        self.ephistarLineedit = QtGui.QLineEdit(self.frame)
        self.ephistarLineedit.setObjectName("ephistarLineedit")
        self.gridLayout_2.addWidget(self.ephistarLineedit, 0, 1, 1, 1)
        self.ephistarLabel_2 = QtGui.QLabel(self.frame)
        self.ephistarLabel_2.setObjectName("ephistarLabel_2")
        self.gridLayout_2.addWidget(self.ephistarLabel_2, 0, 2, 1, 1)
        self.eLstarLabel = QtGui.QLabel(self.frame)
        self.eLstarLabel.setObjectName("eLstarLabel")
        self.gridLayout_2.addWidget(self.eLstarLabel, 1, 0, 1, 1)
        self.eLstarLineedit = QtGui.QLineEdit(self.frame)
        self.eLstarLineedit.setObjectName("eLstarLineedit")
        self.gridLayout_2.addWidget(self.eLstarLineedit, 1, 1, 1, 1)
        self.eLstarLabel_2 = QtGui.QLabel(self.frame)
        self.eLstarLabel_2.setObjectName("eLstarLabel_2")
        self.gridLayout_2.addWidget(self.eLstarLabel_2, 1, 2, 1, 1)
        self.ealphaLabel = QtGui.QLabel(self.frame)
        self.ealphaLabel.setObjectName("ealphaLabel")
        self.gridLayout_2.addWidget(self.ealphaLabel, 2, 0, 1, 1)
        self.ealphaLineedit = QtGui.QLineEdit(self.frame)
        self.ealphaLineedit.setObjectName("ealphaLineedit")
        self.gridLayout_2.addWidget(self.ealphaLineedit, 2, 1, 1, 1)
        self.ealphaLabel_2 = QtGui.QLabel(self.frame)
        self.ealphaLabel_2.setObjectName("ealphaLabel_2")
        self.gridLayout_2.addWidget(self.ealphaLabel_2, 2, 2, 1, 1)
        self.redshift_limLabel = QtGui.QLabel(self.frame)
        self.redshift_limLabel.setObjectName("redshift_limLabel")
        self.gridLayout_2.addWidget(self.redshift_limLabel, 3, 0, 1, 1)
        self.redshift_limLineedit = QtGui.QLineEdit(self.frame)
        self.redshift_limLineedit.setObjectName("redshift_limLineedit")
        self.gridLayout_2.addWidget(self.redshift_limLineedit, 3, 1, 1, 1)
        self.horizontalLayout.addWidget(self.frame)
        self.horizontalLayout_4.addLayout(self.horizontalLayout)
        self.gridLayout_3.addLayout(self.horizontalLayout_4, 4, 0, 1, 3)
        self.label_3 = QtGui.QLabel(LuminosityFunctionDlg)
        font = QtGui.QFont()
        font.setWeight(75)
        font.setBold(True)
        self.label_3.setFont(font)
        self.label_3.setObjectName("label_3")
        self.gridLayout_3.addWidget(self.label_3, 5, 0, 1, 1)
        self.formLayout = QtGui.QFormLayout()
        self.formLayout.setObjectName("formLayout")
        self.kcorrcataLabel = QtGui.QLabel(LuminosityFunctionDlg)
        self.kcorrcataLabel.setObjectName("kcorrcataLabel")
        self.formLayout.setWidget(0, QtGui.QFormLayout.LabelRole, self.kcorrcataLabel)
        self.kcorrcataComboBox = QtGui.QComboBox(LuminosityFunctionDlg)
        self.kcorrcataComboBox.setObjectName("kcorrcataComboBox")
        self.formLayout.setWidget(0, QtGui.QFormLayout.FieldRole, self.kcorrcataComboBox)
        self.galayxytypeLabel = QtGui.QLabel(LuminosityFunctionDlg)
        self.galayxytypeLabel.setObjectName("galayxytypeLabel")
        self.formLayout.setWidget(1, QtGui.QFormLayout.LabelRole, self.galayxytypeLabel)
        self.galaxytypeComboBox = QtGui.QComboBox(LuminosityFunctionDlg)
        self.galaxytypeComboBox.setObjectName("galaxytypeComboBox")
        self.formLayout.setWidget(1, QtGui.QFormLayout.FieldRole, self.galaxytypeComboBox)
        self.kcorrfilterLabel = QtGui.QLabel(LuminosityFunctionDlg)
        self.kcorrfilterLabel.setObjectName("kcorrfilterLabel")
        self.formLayout.setWidget(2, QtGui.QFormLayout.LabelRole, self.kcorrfilterLabel)
        self.kcorrfilterComboBox = QtGui.QComboBox(LuminosityFunctionDlg)
        self.kcorrfilterComboBox.setObjectName("kcorrfilterComboBox")
        self.formLayout.setWidget(2, QtGui.QFormLayout.FieldRole, self.kcorrfilterComboBox)
        self.gridLayout_3.addLayout(self.formLayout, 6, 0, 1, 2)
        self.label_6 = QtGui.QLabel(LuminosityFunctionDlg)
        font = QtGui.QFont()
        font.setWeight(75)
        font.setBold(True)
        self.label_6.setFont(font)
        self.label_6.setObjectName("label_6")
        self.gridLayout_3.addWidget(self.label_6, 7, 0, 1, 1)
        self.horizontalLayout_7 = QtGui.QHBoxLayout()
        self.horizontalLayout_7.setObjectName("horizontalLayout_7")
        spacerItem2 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_7.addItem(spacerItem2)
        self.extinction_lineedit = QtGui.QLineEdit(LuminosityFunctionDlg)
        self.extinction_lineedit.setObjectName("extinction_lineedit")
        self.horizontalLayout_7.addWidget(self.extinction_lineedit)
        self.label = QtGui.QLabel(LuminosityFunctionDlg)
        self.label.setObjectName("label")
        self.horizontalLayout_7.addWidget(self.label)
        self.gridLayout_3.addLayout(self.horizontalLayout_7, 8, 0, 1, 2)
        spacerItem3 = QtGui.QSpacerItem(174, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.gridLayout_3.addItem(spacerItem3, 8, 2, 1, 1)
        self.buttonBox = QtGui.QDialogButtonBox(LuminosityFunctionDlg)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok|QtGui.QDialogButtonBox.Reset)
        self.buttonBox.setObjectName("buttonBox")
        self.gridLayout_3.addWidget(self.buttonBox, 9, 0, 1, 3)

        self.retranslateUi(LuminosityFunctionDlg)
        QtCore.QObject.connect(self.pushButton, QtCore.SIGNAL("toggled(bool)"), self.frame.setEnabled)
        QtCore.QMetaObject.connectSlotsByName(LuminosityFunctionDlg)
        LuminosityFunctionDlg.setTabOrder(self.LFnameLineedit, self.phistarLineedit)
        LuminosityFunctionDlg.setTabOrder(self.phistarLineedit, self.LstarLineedit)
        LuminosityFunctionDlg.setTabOrder(self.LstarLineedit, self.unitComboBox)
        LuminosityFunctionDlg.setTabOrder(self.unitComboBox, self.alphaLineedit)
        LuminosityFunctionDlg.setTabOrder(self.alphaLineedit, self.galaxytypeComboBox)
        LuminosityFunctionDlg.setTabOrder(self.galaxytypeComboBox, self.extinction_lineedit)
        LuminosityFunctionDlg.setTabOrder(self.extinction_lineedit, self.buttonBox)
        LuminosityFunctionDlg.setTabOrder(self.buttonBox, self.ephistarLineedit)
        LuminosityFunctionDlg.setTabOrder(self.ephistarLineedit, self.eLstarLineedit)
        LuminosityFunctionDlg.setTabOrder(self.eLstarLineedit, self.ealphaLineedit)
        LuminosityFunctionDlg.setTabOrder(self.ealphaLineedit, self.redshift_limLineedit)

    def retranslateUi(self, LuminosityFunctionDlg):
        LuminosityFunctionDlg.setWindowTitle(QtGui.QApplication.translate("LuminosityFunctionDlg", "Dialog", None, QtGui.QApplication.UnicodeUTF8))
        self.LFnameLabel.setText(QtGui.QApplication.translate("LuminosityFunctionDlg", "Galaxy population", None, QtGui.QApplication.UnicodeUTF8))
        self.label_2.setText(QtGui.QApplication.translate("LuminosityFunctionDlg", "Luminosity function", None, QtGui.QApplication.UnicodeUTF8))
        self.label_4.setText(QtGui.QApplication.translate("LuminosityFunctionDlg", "Filter", None, QtGui.QApplication.UnicodeUTF8))
        self.phistarLabel.setText(QtGui.QApplication.translate("LuminosityFunctionDlg", "Phi_star", None, QtGui.QApplication.UnicodeUTF8))
        self.LstarLabel.setText(QtGui.QApplication.translate("LuminosityFunctionDlg", "L_star/M_star", None, QtGui.QApplication.UnicodeUTF8))
        self.alphaLabel.setText(QtGui.QApplication.translate("LuminosityFunctionDlg", "alpha", None, QtGui.QApplication.UnicodeUTF8))
        self.pushButton.setText(QtGui.QApplication.translate("LuminosityFunctionDlg", "Evolution", None, QtGui.QApplication.UnicodeUTF8))
        self.ephistarLabel.setText(QtGui.QApplication.translate("LuminosityFunctionDlg", "e_Phi_star", None, QtGui.QApplication.UnicodeUTF8))
        self.ephistarLabel_2.setText(QtGui.QApplication.translate("LuminosityFunctionDlg", "TODO: equ", None, QtGui.QApplication.UnicodeUTF8))
        self.eLstarLabel.setText(QtGui.QApplication.translate("LuminosityFunctionDlg", "e_L_star/M_star", None, QtGui.QApplication.UnicodeUTF8))
        self.eLstarLabel_2.setText(QtGui.QApplication.translate("LuminosityFunctionDlg", "TODO: eq", None, QtGui.QApplication.UnicodeUTF8))
        self.ealphaLabel.setText(QtGui.QApplication.translate("LuminosityFunctionDlg", "e_alpha", None, QtGui.QApplication.UnicodeUTF8))
        self.ealphaLabel_2.setText(QtGui.QApplication.translate("LuminosityFunctionDlg", "TODO: eq", None, QtGui.QApplication.UnicodeUTF8))
        self.redshift_limLabel.setText(QtGui.QApplication.translate("LuminosityFunctionDlg", "z limit", None, QtGui.QApplication.UnicodeUTF8))
        self.label_3.setText(QtGui.QApplication.translate("LuminosityFunctionDlg", "KCorrection", None, QtGui.QApplication.UnicodeUTF8))
        self.kcorrcataLabel.setText(QtGui.QApplication.translate("LuminosityFunctionDlg", "Catalog", None, QtGui.QApplication.UnicodeUTF8))
        self.galayxytypeLabel.setText(QtGui.QApplication.translate("LuminosityFunctionDlg", "Galaxy type", None, QtGui.QApplication.UnicodeUTF8))
        self.kcorrfilterLabel.setText(QtGui.QApplication.translate("LuminosityFunctionDlg", "Filter", None, QtGui.QApplication.UnicodeUTF8))
        self.label_6.setText(QtGui.QApplication.translate("LuminosityFunctionDlg", "Extinction", None, QtGui.QApplication.UnicodeUTF8))
        self.label.setText(QtGui.QApplication.translate("LuminosityFunctionDlg", "mag", None, QtGui.QApplication.UnicodeUTF8))

