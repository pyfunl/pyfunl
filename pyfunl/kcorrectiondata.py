#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (c) 2009 Nicolas Gruel All rights reserved.
# This program or module is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, 
# or (at your option) any later version. It is distributed in the hope that
# it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
# the GNU General Public License for more details.

import re
try:
    import numpy as np
except:
    raise 'numpy not installed'
    
#TODO: Change it to have better data management and possibility that the user give a file
kcorrect_files = {'Coleman 1980' : '/home/gruel/.pyfunl/data/col.dat', 
                  'Poggianti 1997' : '/home/gruel/.pyfunl/data/pog.dat'}

def read(catalog):
    f = open(kcorrect_files[catalog])
    
    __kcor = f.readlines()
    f.close()

    filter_start = []
    for i, line in enumerate(__kcor):
        if 'filter' in line:
            filter_start.append(i)
    filter_start.append(len(__kcor)+1)
    
    kcor = {}
    for i in range(len(filter_start)-1):
        dep = filter_start[i]
        end = filter_start[i+1]-1

        kcor[__kcor[dep].strip()] = [re.split('\t',line.strip()) for line in __kcor[dep+1:end]]
    for key in kcor.keys():
        kcor[key] = np.array(kcor[key],dtype=np.float)
    kcor['columns'] = __kcor[1][1:].strip().split()
    return kcor
        
    