#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (c) 2009 Nicolas Gruel All rights reserved.
# This program or module is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, 
# or (at your option) any later version. It is distributed in the hope that
# it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
# the GNU General Public License for more details.

import sys

from PyQt4.QtCore import *
from PyQt4.QtGui import *

import ui_metrics

#try:
#    a = '{0}'.format('0')
#    version = '>2.6'
#except:
#    version = '2.5'

__py_version__ = float(sys.version[:3]) #remplacer par sys.version_info
    
try:
    import compatibility.metrics as coscalc
except ImportError:
    import coscalc

MAC = True
try:
    from PyQt4.QtGui import qt_mac_set_native_menubar
except ImportError:
    MAC = False

flrw = coscalc.Flrw(71,0.27,0.73)

class Metrics(QMainWindow, ui_metrics.Ui_Metrics):
    
    def __init__(self, parent=None):
        super(Metrics, self).__init__(parent)

        self.__H0 = flrw.get_hubble()
        self.__OmegaM = flrw.get_matter()
        self.__OmegaL = flrw.get_vacuum()
   
        self.setupUi(self)
        self.matterLabel.setText(u'\u03a9M')
        self.vacuumLabel.setText(u'\u03a9\u03bb')
        self.redshiftLabel.setText('z')
                
        #Result you want to have.
        self.metrics_method = [self.ageCheckBox, self.ltCheckBox, self.dcCheckBox, self.dmCheckBox,
                               self.dlCheckBox, self.DMCheckBox, self.daCheckBox, self.volCheckBox,
                               self.angularscaleCheckBox]
        
        self.checkbox = {self.ageCheckBox : ['Age of the universe', flrw.age, 'Gyr'],
                         self.ltCheckBox : ['Look Back Time',flrw.lt, 'Gyr'],
                         self.dcCheckBox : ['Line of sight comoving distance',flrw.dc, 'Mpc'],
                         self.dmCheckBox : ['Transverse comoving distance', flrw.dm, 'Mpc'],
                         self.dlCheckBox : ['Luminosity distance',flrw.dl, 'Mpc'],
                         self.DMCheckBox : ['Distance Modulus',flrw.DM, 'mag'],
                         self.daCheckBox : ['Angular distance',flrw.da, 'Mpc'],
                         self.volCheckBox : ['Comoving volume',flrw.vol, 'Gpc<sup>3</sup>/sr'],
                         self.angularscaleCheckBox : ['angular scale', flrw.angular_scale, 'pc'],
                         }

        self.defaultForm()
        self.metrics_container = []

        if not MAC:            
            self.buttonBox.setFocusPolicy(Qt.NoFocus)        

        self.connect(self.hubbleEdit,SIGNAL('returnPressed()'), self.returnHubble)
        self.connect(self.matterEdit,SIGNAL('returnPressed()'), self.returnMatter)
        self.connect(self.vacuumEdit,SIGNAL('returnPressed()'), self.returnVacuum)

        self.redshiftEdit.setFocus()        
        self.connect(self.redshiftEdit,SIGNAL('returnPressed()'), self.returnRedshift)

        self.connect(self.ageCheckBox, SIGNAL('returnPressed()'), self.ltCheckBox.setFocus)

        self.connect(self.buttonBox.button(QDialogButtonBox.Reset), SIGNAL('clicked()'), self.resetForm)        
        self.connect(self.buttonBox.button(QDialogButtonBox.Apply), SIGNAL('clicked()'), self.apply)
        self.connect(self.buttonBox.button(QDialogButtonBox.Close), SIGNAL('clicked()'), SLOT('close()'))
    
        self.connect(self.CheckAllButton,SIGNAL('clicked()'),self.checkall)
        self.connect(self.UncheckAllButton,SIGNAL('clicked()'),self.uncheckall)
        #status bar
        status = self.statusBar()
        status.showMessage("Ready", 5000)

    def validateHubble(self):
        '''Update Age in function of the cosmology'''
        
        try: 
            if self.hubbleEdit.text().isEmpty() or self.hubbleEdit.text() == ' ':
                raise ValueError, 'Please enter a Hubble parameter'                
            self.H0 = float(self.hubbleEdit.text())
            if self.H0 <= 0:
                raise ValueError, 'Hubble constant <= 0 not allowed'
            #to be compatible with milia
#            if hasattr(flrw, 'hubble'):
#                flrw.hubble = self.H0
#            else:
            flrw.set_hubble(self.H0)
            return True
        except ValueError, e:
            QMessageBox.warning(self,'Hubble constant error', unicode(e))
            self.hubbleEdit.setText('')
            self.hubbleEdit.selectAll()
            self.hubbleEdit.setFocus()
            return False
        
    def returnHubble(self):
        self.validateHubble()
        if self.hubbleEdit.text().isEmpty() or self.hubbleEdit.text() == ' ':
            self.hubbleEdit.setFocus()
        else:  
            self.matterEdit.setFocus()
    
    def validateMatter(self):
        '''Update Matter density in function of the cosmology given'''
        try: 
            if self.matterEdit.text().isEmpty() or self.matterEdit.text() == ' ':
                raise ValueError, 'Please enter a matter density value'                
            self.OmegaM = float(self.matterEdit.text())
            if self.OmegaM < 0:
                raise ValueError, 'Matter density < 0 not allowed'
            #to be compatible with milia
#            if hasattr(flrw, 'matter'):
#                flrw.matter = self.OmegaM
#            else:
            flrw.set_matter(self.OmegaM)
            return True        
        except ValueError, e:
            QMessageBox.warning(self,'Matter density error', unicode(e))
            self.matterEdit.setText('')
            self.matterEdit.selectAll()
            self.matterEdit.setFocus()
            return False

    def returnMatter(self):
        self.validateMatter()
        if self.matterEdit.text().isEmpty() or self.matterEdit.text() == ' ':
            self.matterEdit.setFocus()
        else:  
            self.vacuumEdit.setFocus()
                
    def validateVacuum(self):
        '''Update Vacuum density in function of the cosmology'''
        try:
            if self.vacuumEdit.text().isEmpty() or self.vacuumEdit.text() == ' ':
                print self.vacuumEdit.text().isEmpty(), 
                raise ValueError, 'Please enter a vacuum density value'                            
            self.OmegaL = float(self.vacuumEdit.text())
            if self.OmegaL < 0:
                raise ValueError, 'The Universe recolapse'
            #to be compatible with milia
#            if hasattr(flrw, 'vacuum'):
#                flrw.vacuum = self.OmegaL
#            else:
            flrw.set_vacuum(self.OmegaL)
            return True
        except ValueError, e:
            QMessageBox.warning(self,'Vacuum density error: ', unicode(e))
            self.vacuumEdit.selectAll()
            self.vacuumEdit.setFocus()
            return False

    def returnVacuum(self):
        self.validateVacuum()
        if self.vacuumEdit.text().isEmpty():
            self.vacuumEdit.setFocus()
        else:  
            self.redshiftEdit.setFocus()

    def validateRedshift(self):
        '''Update redshift in function of the cosmology'''
        try:
            if self.redshiftEdit.text().isEmpty():
                raise ValueError, 'Please enter a redshift'
            self.redshift = float(self.redshiftEdit.text())
            if self.redshift < 0:
                raise ValueError, 'The redshift must be positif'
            return True
        except ValueError, e:
            QMessageBox.warning(self,'Redshift error', unicode(e))
            self.redshiftEdit.selectAll()
            self.redshiftEdit.setFocus()
            return False
        
    def returnRedshift(self):
        self.validateRedshift()
        if self.redshiftEdit.text().isEmpty():
            self.redshiftEdit.setFocus()
        else:  
            self.ageCheckBox.setFocus()
            
    def updateAge(self):
        '''Update Age when the cosmology is changed'''
        if __py_version__ < 2.6:
            self.agez0resultLabel.setText('%.2f Gyr' % flrw.age())
        else:
            self.agez0resultLabel.setText('{0:.2f} Gyr'.format(flrw.age()))

    def checkall(self):
        '''Check all the checkbox'''
        for key in self.checkbox.keys():
            key.setChecked(True)

    def uncheckall(self):
        '''Check all the checkbox'''
        for key in self.checkbox.keys():
            key.setChecked(False)

    def defaultForm(self):
        '''Reset the parameters at their default value '''
                
        self.H0 = float(self.__H0)
        flrw.set_hubble(self.H0)
        self.OmegaM = float(self.__OmegaM)
        flrw.set_matter(self.OmegaM)
        self.OmegaL = float(self.__OmegaL)
        flrw.set_vacuum(self.OmegaL)

        if __py_version__ < 2.6: 
            self.hubbleEdit.setText('%.2f ' % (self.__H0))
            self.matterEdit.setText('%.2f' % (self.__OmegaM))
            self.vacuumEdit.setText('%.2f' % (self.__OmegaL))
        else:
            self.hubbleEdit.setText('{0:.2f}'.format(self.__H0))    
            self.matterEdit.setText('{0:.2f}'.format(self.__OmegaM))
            self.vacuumEdit.setText('{0:.2f}'.format(self.__OmegaL))
 
        self.updateAge()
        #self.textEdit.clear()
        self.redshiftEdit.setText('')
        self.redshift = None        

        self.uncheckall()

        self.row = 0
        #initialization header and data for tableWidget (should be doable cleaner) TODO:
        header = ['H0' , u'\u03a9M',u'\u03a9\u03bb','z']
        data = [self.__H0, self.__OmegaM, self.__OmegaL, '']

        if hasattr(self,'tableWidget'):
            self.tableWidget.clear()
            self.tableWidget.setColumnCount(4)
            self.tableWidget.setRowCount(1)
            self.tableWidget.setHorizontalHeaderLabels(header)
        
            for col in range(len(header)):

                if __py_version__ < 2.6:
                    item = QTableWidgetItem('%s' % (data[col]))
                else:
                    item = QTableWidgetItem('{0}'.format(data[col]))

                item.setTextAlignment(Qt.AlignRight|Qt.AlignVCenter)
                self.tableWidget.setItem(0,col,item)
            
            self.tableWidget.resizeColumnsToContents()

    def resetForm(self):
        '''Reset the parameters at their default value '''
                
        flrw.set_hubble(None)
        flrw.set_matter(None)
        flrw.set_vacuum(None)

        self.hubbleEdit.setText('')
        self.matterEdit.setText('')
        self.vacuumEdit.setText('')
 
        self.agez0resultLabel.setText('')
        self.redshiftEdit.setText('')
        self.redshift = None

        self.uncheckall()

        self.row = 0
        #initialization header and data for tableWidget (should be doable cleaner) TODO:
        header = ['H0' , u'\u03a9M',u'\u03a9\u03bb','z']
        data = ['', '', '', '']
        self.tableWidget.clear()

        if hasattr(self,'tableWidget'):
            self.tableWidget.clear()
            self.tableWidget.setColumnCount(4)
            self.tableWidget.setRowCount(1)
            self.tableWidget.setHorizontalHeaderLabels(header)
        
            for col in range(len(header)):

                if __py_version__ < 2.6:
                    item = QTableWidgetItem('%s' % (data[col]))
                else:
                    item = QTableWidgetItem('{0}'.format(data[col]))

                item.setTextAlignment(Qt.AlignRight|Qt.AlignVCenter)
                self.tableWidget.setItem(0,col,item)
            
            self.tableWidget.resizeColumnsToContents()


    def apply(self):
        '''Launch the calcul and verification'''
        #TODO: A finir changer la ou le resultat s'affiche! Mettre sous forme de table

        if not self.validateHubble():
            return        
        if not self.validateMatter():
            return
        if not self.validateVacuum():
            return
        if not self.validateRedshift():
            return
        self.checkbox[self.angularscaleCheckBox][0] = 'Angular scale : 1"'
        self.updateAge()
        if hasattr(self,'tableWidget'):
            self.updateTable()
        #self.updateTextEdit()
        
    def updateTextEdit(self):
        '''Update TextEdit widget.
        '''
        for key in self.metrics_method: #self.checkbox.keys():
            if key.isChecked():                
                if __py_version__ < 2.6:
                    if self.checkbox[key][1] == flrw.vol:
                        self.textEdit.insertHtml('%s = %.6f  %s<br>' % 
                                                 (self.checkbox[key][0], 
                                                  self.checkbox[key][1](self.redshift)*1e-9, 
                                                  self.checkbox[key][2]))
                    else:
                        self.textEdit.insertHtml('%s = %.6f  %s<br>' % 
                                                 (self.checkbox[key][0], 
                                                  self.checkbox[key][1](self.redshift),
                                                  self.checkbox[key][2]))
                else:
                    if self.checkbox[key][1] == flrw.vol:
                        self.textEdit.insertHtml('{0:s} = {1:.6f} {2:s}<br>'.format(
                                                 self.checkbox[key][0],
                                                 self.checkbox[key][1](self.redshift)*1e-9,
                                                 self.checkbox[key][2]))
                    else:
                        self.textEdit.insertHtml('{0:s} = {1:.6f} {2:s}<br>'.format(
                                                 self.checkbox[key][0], 
                                                 self.checkbox[key][1](self.redshift),
                                                 self.checkbox[key][2]))

#    def updateTable(self):
#        '''
#        Method to update the Table view.
#        '''
#        self.row += 1
#        #initialization header and data for tableWidget (should be doable cleaner) TODO:
#        header = ['H0' , u'\u03a9M',u'\u03a9\u03bb','z']
#        data = [flrw.get_hubble(),flrw.get_matter(),flrw.get_vacuum(),self.redshift] 
#  
#        for key in self.metrics_method: #self.checkbox.keys():
#            if key.isChecked():
#                if self.checkbox[key][1] == flrw.vol:
#                    header.append(self.checkbox[key][0]+'\n'+self.checkbox[key][2])
#                    if __py_version__ < 2.6:
#                        data.append('%.6f' % self.checkbox[key][1](self.redshift)*1e-9)
#                    else:
#                        data.append('{0:.6f}'.format(self.checkbox[key][1](self.redshift)*1e-9))
#                else:
#                    header.append(self.checkbox[key][0]+'\n'+self.checkbox[key][2])
#                    if __py_version__ < 2.6:
#                        data.append('%.6f' % self.checkbox[key][1](self.redshift))
#                    else:
#                        data.append('{0:.6f}'.format(self.checkbox[key][1](self.redshift)))
#
#        #TODO: faire test que ce sont les memes headers si plusieurs fois un calcul
#        self.tableWidget.clear()
#        self.tableWidget.setColumnCount(len(header))
#        self.tableWidget.setRowCount(self.row)
#        self.tableWidget.setHorizontalHeaderLabels(header)
#
#        for row in range(self.row):
#            for col in range(len(header)):            
#                if __py_version__ < 2.6:
#                    item = QTableWidgetItem('%s' % (data[col]))
#                else:
#                    item = QTableWidgetItem('{0}'.format(data[col]))
#
#                item.setTextAlignment(Qt.AlignRight|Qt.AlignVCenter)
#                self.tableWidget.setItem(row,col,item)
#            
#        self.tableWidget.resizeColumnsToContents()

    def updateTable(self):
        r'''
        Method to update the Table view.

        TODO: Change how everything is done and use container and yield!
        TODO: Check si deux fois la meme cosmo et le meme redshift.
        '''
        
        self.row += 1

        #initialization header and data for tableWidget (should be doable cleaner) TODO:
        header = ['H0' , u'\u03a9M',u'\u03a9\u03bb','z']
        data = [flrw.get_hubble(),flrw.get_matter(),flrw.get_vacuum(),self.redshift] 
  
        for key in self.metrics_method: #self.checkbox.keys():
            if key.isChecked():
                if self.checkbox[key][1] == flrw.vol:
                    header.append(self.checkbox[key][0]+'\n'+self.checkbox[key][2])
                    if __py_version__ < 2.6:
                        data.append('%.6f' % self.checkbox[key][1](self.redshift)*1e-9)
                    else:
                        data.append('{0:.6f}'.format(self.checkbox[key][1](self.redshift)*1e-9))
                else:
                    header.append(self.checkbox[key][0]+'\n'+self.checkbox[key][2])
                    if __py_version__ < 2.6:
                        data.append('%.6f' % self.checkbox[key][1](self.redshift))
                    else:
                        data.append('{0:.6f}'.format(self.checkbox[key][1](self.redshift)))
            else:
                header.append(self.checkbox[key][0]+'\n'+self.checkbox[key][2])
                if __py_version__ < 2.6:
                    data.append('---')
                else:
                    data.append('---')

        self.metrics_container.append(data)
        self.tableWidget.clear()
        self.tableWidget.setColumnCount(len(header))
        self.tableWidget.setRowCount(self.row)
        self.tableWidget.setHorizontalHeaderLabels(header)
        
        for row in range(self.row):
            for col in range(len(header)):            
                if __py_version__ < 2.6:
                    item = QTableWidgetItem('%s' % (self.metrics_container[row][col]))
                else:
                    item = QTableWidgetItem('{0}'.format(self.metrics_container[row][col]))

                item.setTextAlignment(Qt.AlignRight|Qt.AlignVCenter)
                self.tableWidget.setItem(row,col,item)
        
        self.tableWidget.resizeColumnsToContents()

    
def main():
    '''Function to start the GUI. Needed by setuptools.
    '''
    app = QApplication(sys.argv)
    metrics = Metrics()
    metrics.show()
    app.exec_()
    
if __name__ == '__main__':
    app = QApplication(sys.argv)
    metrics = Metrics()
    metrics.show()
    app.exec_()
