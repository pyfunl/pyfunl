def readcol(fname,comments='#',delimiter=None,skiprows=None,usecols=None,dtype='list'):
    r'''Load ASCII data from fname into an array and return the array.

    The data must be regular, same number of values in every row fname 
    can be a filename or a file handle.


    Parameters
    ----------

    Fname : string
        Name of the file to read

    comments : string, optional
        Comments delimeter
        Default is the python character '#'.

    delimiter : string, optional 
        Columns delimeter.

    usecols : list, optional 
        list or tuple of integers which contains the columns to use.

    skiprows : integer
        indicate from which line you want to begin
        to read the data file (useful to avoid the descriptions lines)

    dtype : dtype, string
        nympy dtype for the numpy array in which the data or a list if equale to `list`
        Default `list`. 


    Returns
    -------
    data : list, array
        the data are in numpy array or list depending of the dtype parameter

    Examples
    --------
    from numpy import transpose
    x,y = transpose(readcol('test.dat'))  # data in two columns

    X = readcol('test.dat')    # a matrix of data

    x = readcol('test.dat')    # a single column of data

    x = readcol('test.dat,'#') # the character use like a comment delimiter is '#'

    initial function from pylab (J.Hunter). Modification N. Gruel.
    '''
    fh = file(fname)

    X = []
    numCols = None
    if usecols is None:
        for nline,line in enumerate(fh):
            if nline < skiprows: 
                continue
            line = line[:line.find(comments)].strip()
            if not len(line): 
                continue
            row = [val.strip() for val in line.split(delimiter)]
            thisLen = len(row)
            if numCols is not None and thisLen != numCols:
                raise ValueError('All rows must have the same number of columns')
            X.append(row)
    else:
        for nline,line in enumerate(fh):
            if nline < skiprows: 
                continue
            line = line[:line.find(comments)].strip()
            if not len(line): 
                continue
            row = line.split(delimiter)
            row = [row[i].strip() for i in usecols]
            thisLen = len(row)

            if numCols is not None and thisLen != numCols:
                raise ValueError('All rows must have the same number of columns')
            X.append(row)

    try:
        import numpy
        if dtype != 'list':
            X = numpy.array(X)
            r,c = X.shape
            if r==1 or c==1:
                X.shape = max([r,c])
            if dtype != None:
                X = X.astype(dtype)
        fh.close()
        return X
    except:
        fh.close()
        return X
