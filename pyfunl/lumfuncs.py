#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (c) 2009 Nicolas Gruel All rights reserved.
# This program or module is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, 
# or (at your option) any later version. It is distributed in the hope that
# it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
# the GNU General Public License for more details.

# This file is part of pyfunl. API similar to Milia::Schechter from Sergio Pascual

# TODO: add unitest or doctest!!!
from __future__ import division

import sys

from math import exp, log10

try:
    from scipy import integrate
except:
    from integration import closedpoints

import readcol
import astro_unit
import kcorrection

__docformat__ = "restructuredtext en"
__py_version__ = float(sys.version[:3]) #remplacer par sys.version_info

class MyError(Exception):
    r'''Create an personal Error. That print the message I want on screen.
    '''
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

class Schechter(object):
    r'''Class to implement Schechter luminosity function.
    
    This class is compatible with milia.lumfuncs library https://guaix.fis.ucm.es/projects/milia
    
    There are three different constructors for this class: 
    
    - The first constructor with three differents argument ONLY will contains a regular
      Schechter luminosity function with no evolution.
    
    - The second constructor will be as above but a fourth parameter to tell at which redshift 
      this luminosity function is valid.
    
    - The third constructor has seven parameters. It is using a parametrization from Heyl et al 1997 paper
      http://adsabs.harvard.edu/abs/1997MNRAS.285..613H
    
      The evolution of the luminosity function is describe as following:
        
      .. math::
          :nowrap:
        
          \begin{eqnarray}
              \phi^{\star}(z) & = &  \phi^{\star}_0 (1+z)^{\gamma_{\phi}} \\        
              L^{\star}(z) & = & L^{\star}_0 (1+z)^{\gamma_L} \\
              \alpha(z) & = & \alpha_{0} + \gamma_{\alpha}z
          \end{eqnarray}
    
      The normalization and the characteristics luminosity evolve as a power of cosmic time
      while the faint-end slope evolves linearly with redshift.

      The position of the parameters are important. It's in the form:
      (phi_star, e_phi_star, lum_star, e_lum_star, alpha, e_alpha, redshift).
    
    Attributes
    ----------
    
    phi_star : float
        normalization. It's a number per unit of volume.
    e_phi_star : float, optional
        power of the evolution.    
    lum_star : float
        characteristic luminosity.
    e_lum_star : float, optional
        power of the evolution.               
    alpha : float
        faint-end slope parameter
    e_alpha : float, optional
        power of the evolution.                       
    redshift : float, optional
        redshift of the luminosity function initial.
    
    Methods
    -------
    
    Raises
    ------

    Error if number of attributes not as described in the documentation.
    
    Examples
    --------
    
    >>> from math import log10,log    
    >>> import lumfuncs
    >>> 
    >>> h = 0.71 # cosmology
    >>> #B Band
    ... phi_star = 1.6e-2*h**3
    >>> M_star = -19.7+5*log10(h)
    >>> alpha = -1.07
    >>> #solar bolometrique luminosity and bolometric magnitude
    ... M_solar = 4.74
    >>> L_solar = 3.856e33 #erg/s
    >>> L_star = h**-2*10**(0.4*(M_solar-M_star)) * L_solar
    
    '''

    def __init__(self, *args):
        # Constructor without evolution
        try:
            if len(args) == 3 :
                self.phi_star = lambda x: args[0]
                self.lum_star = lambda x: args[1]
                self.alpha = lambda x: args[2]
                self.current_z = 0
            elif len(args) == 4 :
                self.phi_star = lambda x: args[0]
                self.lum_star = lambda x: args[1]
                self.alpha = lambda x: args[2]
                self.current_z = args[3]
            elif len(args) == 7:
                #Constructor with evolution parametrized following Heyl et al 1997
                phi_star = args[0]
                e_phi_star = args[1]
                lum_star = args[2]
                e_lum_star = args[3]
                alpha = args[4]
                e_alpha = args[5]
                self.current_z = args[6]
                
                p0 = phi_star / (1.+self.current_z)**e_phi_star
                l0 = lum_star / (1.+self.current_z)**e_lum_star
                            
                self.phi_star = lambda x: p0*(1 + x)**e_phi_star 
                self.lum_star = lambda x: l0 * (1 + x)**e_lum_star 
                self.alpha = lambda x: alpha + e_alpha*(x-args[6])
                
            else:
                raise MyError('There are only 3, 4 or 7 arguments allowed.\n\n' +
                              'The first constructor is a Schechter LF which can take 3 arguments:\n' + 
                              'phi_star, Lum_star and alpha\n\n'+
                              'The second is a Schechter LF at a certain redshift.'+
                              'It can take four arguments: \n' +
                              'phi_star, Lum_star, alpha and z\n\n'+
                              'The third constructor is taking 7 arguments and represent an evolving LF:\n' +
                              'phi_star, e_phi_star, lum_star, e_lum_star, alpha, e_alpha and z'
                              )
        except MyError as e:
            print 'Exception occurred, value:\n\n', e.value
        
    def evolve(self, z):
        '''Modify the current redshift of the Luminosity function
        '''
        self.current_z = z
    
    def function(self, lum):
        r'''Define Schechter luminosity function.
           
        :math:`\phi(L)dL = \phi^\star(L/L^\star)^\alpha exp(-L/L^\star) d(L/L^\star)`
           
        Parameters
        ----------
        
        lum : float
            luminosity where you want to calcul LF
        
        Returns
        -------
        
        LF : float 
            the value of the luminosity function at lum
        
        Examples
        --------
        >>> from math import log10,log    
        >>> import lumfuncs
        >>> 
        >>> h = 0.71 # cosmology
        >>> phi_star = 1.6e-2*h**3
        >>> M_star = -19.7+5*log10(h)
        >>> alpha = -1.07
        >>> M_solar = 4.74
        >>> L_solar = 3.856e33
        >>> L_star = h**-2*10**(0.4*(M_solar-M_star)) * L_solar
        >>> lf = lumfuncs.Schechter(phi_star,L_star,alpha)
        >>> lf.function(L_star)
        2.3253959757508692e-47
        '''
        x = lum / self.lum_star(self.current_z)
        alpha = self.alpha(self.current_z)
        phi = self.phi_star(self.current_z)
        return phi * x**alpha * exp(-x) / self.lum_star(self.current_z)
    
    def function_normalized(self, x):
        r'''Luminosity function normalized. 
        
        Change the variable :math:`x = L/L^\star` so :math:`d(L/L^\star) = dx`
        '''
        alpha = self.alpha(self.current_z)
        return x**alpha * exp(-x)

    def object_density(self,lum1, lum2):
        r'''Calcul the object density between two different luminosity 
        for a given luminosity function.

        Parameters
        ----------
        
        lum1 : float
        lum2 : float
        
        Returns
        -------
        
        The object density (in :math:`Mpc^{-3}`)
        
        Examples
        --------
        >>> from math import log10,log    
        >>> import lumfuncs
        >>> 
        >>> h = 0.71 # cosmology
        >>> phi_star = 1.6e-2*h**3
        >>> M_star = -19.7+5*log10(h)
        >>> alpha = -1.07
        >>> M_solar = 4.74
        >>> L_solar = 3.856e33
        >>> L_star = h**-2*10**(0.4*(M_solar-M_star)) * L_solar
        >>> lf = lumfuncs.Schechter(phi_star,L_star,alpha)
        >>> lf.object_density(L_star,2*L_star)
        0.0009567055476993946        
        ''' 

        try:
            value, err = integrate.quad(func=self.function, a=lum1, b=lum2)
        except ValueError:
            raise 'Error in Luminosity Function'
        except:
            from integration import closedpoints
            value = closedpoints(self.function,lum1,lum2)
        return value
    
    def parameters (self):
        r'''Return the parameters of the Schechter luminosity function.
        
        Returns
        -------
        parameter : tuple
            contain :math:`\phi^\star`, :math:`L^\star`, :math:`\alpha`
        '''
        return (self.phi_star, self.lum_star, self.alpha)
    
    def __str__(self):
        '''Print the parameter of the Schechter function.
        '''
        return ("schechter(phi*=" + str(self.phi_star(self.current_z)) 
                + ", lum*=" + str(self.lum_star(self.current_z)) 
                + ", alpha=" + str(self.alpha(self.current_z)) 
                + ", z=" + str(self.current_z) + ")")
        
    def number_objects(self, flrw, z1, z2, l1, l2):
        #TODO: update the docstring
        r'''Calcul the number of object between two different luminosity
        and a range of redshift by steradian. 
        
        Parameters
        ----------
        flrw : class instance
            An instance of the class Flrw
        z1, z2: float
            the redshift interval
        l1, l2: float
            The luminosity cut-off
                
        Returns
        -------
        number_objects : float
            The number of object
            
        Examples
        --------

        >>> import coscalc                                                                                    
        >>> flrw = coscalc.Flrw(71,0.27,0.73)                                                                
        >>> from math import log10,log                                                                        
        >>> import lumfuncs                                                                                   
        >>>                                                                                                   
        >>> h = 0.71 # cosmology                                                                              
        >>> phi_star = 1.6e-2*h**3                                                                            
        >>> M_star = -19.7+5*log10(h)                                                                         
        >>> alpha = -1.07                                                                                     
        >>> M_solar = 4.74                                                                                    
        >>> L_solar = 3.856e33                                                                                
        >>> L_star = h**-2*10**(0.4*(M_solar-M_star)) * L_solar                                               
        >>> lf = lumfuncs.Schechter(phi_star,L_star,alpha)                                                   
        >>> lf.object_density(1e40,L_star)                                                                   
        0.068053811921570631                                                                                  
        >>> lf.number_objects(flrw,0,1,1e40,L_star)                                                         
        828185023.47047174                                                                                    
        >>> import astro_unit                                                                                 
        >>> astro_unit.str2arcsec2(lf.number_objects(flrw,0,1,1e40,L_star))                                 
        0.019466017357682851
        '''

        try:
            vol1 = flrw.vol(z1)
            vol2 = flrw.vol(z2)
        except AttributeError:
            raise AttributeError, 'You need to provide a cosmology instance as a first parameter.'
        return (vol2-vol1)*self.object_density(l1,l2)

    def number_objects2(self, flrw, z1, z2, m1, M2, fwhm):
        #TODO: update the docstring
        r'''Calcul the number of object between two different luminosity
        and a range of redshift by steradian. 
        
        Parameters
        ----------
        flrw : class instance
            An instance of the class Flrw
        z1, z2: float
            the redshift interval
        l1, l2: float
            The luminosity cut-off
                
        Returns
        -------
        number_objects : float
            The number of object
            
        Examples
        --------

        >>> import coscalc                                                                                    
        >>> flrw = coscalc.Flrw(71,0.27,0.73)                                                                
        >>> from math import log10,log                                                                        
        >>> import lumfuncs                                                                                   
        >>>                                                                                                   
        >>> h = 0.71 # cosmology                                                                              
        >>> phi_star = 1.6e-2*h**3                                                                            
        >>> M_star = -19.7+5*log10(h)                                                                         
        >>> alpha = -1.07                                                                                     
        >>> M_solar = 4.74                                                                                    
        >>> L_solar = 3.856e33                                                                                
        >>> L_star = h**-2*10**(0.4*(M_solar-M_star)) * L_solar                                               
        >>> lf = lumfuncs.Schechter(phi_star,L_star,alpha)                                                   
        >>> lf.object_density(1e40,L_star)                                                                   
        0.068053811921570631                                                                                  
        >>> lf.number_objects(flrw,0,1,1e40,L_star)                                                         
        828185023.47047174                                                                                    
        >>> import astro_unit                                                                                 
        >>> astro_unit.str2arcsec2(lf.number_objects(flrw,0,1,1e40,L_star))                                 
        0.019466017357682851
        '''
        try:
            vol1 = flrw.vol(z1)
            vol2 = flrw.vol(z2)
        except AttributeError:
            raise AttributeError, 'You need to provide a cosmology instance as a first parameter.'

        M1 = m1-5*(log10(flrw.dl(0.0001)*1e6)-1)
        l1 = astro_unit.magabs2ergs(M1,fwhm)
        l2 = astro_unit.magabs2ergs(M2,fwhm)
        return (vol2-vol1)*self.object_density(l1,3*l2)

    def object_number(self, flrw, z1, z2, m1, M2, fwhm,  
                      catalog=None, filter=None, galaxytype=None, zlim=0, N=100):
        '''
        
        Parameters
        ----------
        
        N : integer, optional
        
        FIXME: There are a problem in this function.
        
        TODO: N == decide it by mag! 
        TODO: finir d'implemeter zlim
        '''
        if z1>z2:
            tmp = z1
            z1 = z2
            z2 = tmp
        
        Dz = (z2-z1)/N
        N_i = []
        vol = []
        for i in range(N, 0, -1):
            vol_i = flrw.vol(z1+i*Dz)-flrw.vol(z1+(i-1)*Dz)
            z_mi = z1+(i-1)*Dz+Dz/2
            if z_mi == 0:
                if catalog is not None:
                    m1 += kcorrection.kcorrec(0.0001, catalog, filter, galaxytype)
                M1 = m1-5*(log10(flrw.dl(0.0001)*1e6)-1)
            else:
                if catalog is not None:
                    m1 += kcorrection.kcorrec(z_mi, catalog, filter, galaxytype)
                M1 = m1-5*(log10(flrw.dl(z_mi)*1e6)-1)
                
            l1 = astro_unit.magabs2ergs(M1,fwhm)
            l2 = astro_unit.magabs2ergs(M2,fwhm)
                            
            if z_mi > zlim:
                self.evolve(z_mi)

            vol.append(vol_i)
            tmp = self.object_density(l1,l2)
            if tmp < 0:
                tmp = 0
            N_i.append(vol_i*tmp)
        
        return N_i


    def object_number2(self, flrw, z1, z2, m1, M2, fwhm, zlim=0, N=100):
        '''
        
        Parameters
        ----------
        
        N : integer, optional
        
        FIXME: There are a problem in this function.
        
        TODO: N == decide it by mag! 
        TODO: finir d'implemeter zlim
        '''
        if z1>z2:
            tmp = z1
            z1 = z2
            z2 = tmp

        l2 = astro_unit.magabs2ergs(M2,fwhm)        
        
        Dz = (z2-z1)/N
        N_i = []
        vol = []
        for i in range(N, -1, -1):
            vol_i = flrw.vol(z1+i*Dz)-flrw.vol(z1+(i-1)*Dz)
            z_mi = z1+i*Dz
            if z_mi == 0:
                M1 = m1-5*(log10(flrw.dl(0.0001)*1e6)-1)
            else:                
                M1 = m1-5*(log10(flrw.dl(z_mi)*1e6)-1)
            
            l1 = astro_unit.magabs2ergs(M1,fwhm)
            print M1, l1
            if z_mi > zlim:
                self.evolve(z_mi)
                
            tmp = self.object_density(l1,l2)
            if tmp < 0:
                tmp = 0
        
            vol.append(vol_i)
            N_i.append(vol_i*tmp)    
        return N_i


class Table(object):
    r'''Luminosity function is define by a table not a function.
    '''
    def __init__(self, namefile, format=None, unit=None):
        #Constructor for 
        self.namefile= namefile
        self.format = format
        self.unit = unit
        
    def read(self):
        r'''Read the luminosity function from a file.
        
        Parameters
        ----------
        
        Returns
        -------
        
        Examples
        --------
        
        '''
        try:
            import numpy as np
            dtype = np.float
        except:
            dtype = 'list'
            
        self.data = readcol.readcol(self.namefile, dtype=dtype)
        
    def mag2lum(self):
        '''
        
        '''
        self.data = [astro_unit.mag2lum(mag) for mag in self.data]
            