#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (c) 2009 Nicolas Gruel All rights reserved.
# This program or module is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, 
# or (at your option) any later version. It is distributed in the hope that
# it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
# the GNU General Public License for more details.

# This file is part of pyfunl
from __future__ import division

r'''This module is made to do something similar than 
the NICMOS unit conversion webpage [1]_. (Not true it's far simpler 
I limit the scope of it for pyfunl).
 

NICMOS code is not public and so I am using directly the formula 
gave by the two Instrument Science Report [2]_ and [3]_.

There are a known bug (see file in doc for G3D) in the conversion.

Other links interesting: 

References
----------
    .. [1] http://www.stsci.edu/hst/nicmos/tools/conversion_form.html

    .. [2] NICMOS-014: http://www.stsci.edu/hst/nicmos/documents/isrs/isr_014.pdf

    .. [3] NICMOS-016: http://www.stsci.edu/hst/nicmos/documents/isrs/isr_016.pdf

    .. [4] http://www.astro.virginia.edu/class/oconnell/astr511/lec14-f03.pdf

    .. [5] http://www.sdss.org/DR2/algorithms/fluxcal.html

    .. [6] http://archive.eso.org/apps/mag2flux/help/help.html

    .. [7] http://www.astro.utoronto.ca/~patton/astro/mags.html

    .. [8] Binney and Merifield, Galactic Astronomy, p164
    
'''
from math import log10, pi

phys_const = {
              #physics constant (SI)
              'c_si' : 299792458.0,       # m/s

              #physics constant (cgs)
              'c_cgs' : 2.99792458e10,       # speed of light (cm/s)
              'k_cgs' : 1.380658e-16,      # Boltzmann constant (erg K)
              'h_cgs' : 6.626076e-27,      # Planck constant (erg s)
              }

#unit conversion
conversion = {#angle 
              'str2arcsec2' : (180*60*60/pi)**2,
              #Distance
              'm2cm' : 1e2,
              'm2km' : 1e-3,
              'm2aa' : 1e10,
              'm2tocm2' : 1e4,
              'cm2m' : 1e-2,
              'cm2tom2' : 1e-4,
              'cm2aa' : 1e8,
              'aa2cm' : 1e-8,
              'aa2nm' : 1e-1,
              'aa2m' : 1e-10,
              'aa2mum' : 1e-4,
              'nm2aa' : 1e1,
              'mum2aa' : 1e4,
              #1 Astronomical Unit = 149 598 000 000 meters = 149 597 870 691 ± 30 m
              'ua2cm' : 149.598e11,
              #1 parsec = 206.264.806248 UA
              'pc2ua' : 206264.806248,
              'jy2si' : 10**-26, # Jy -> W / (m^2 * Hz) 
              'jy2cgs' : 10**-23, # Jy -> erg / (s * cm^2 * Hz)  
              }

LF_unit = ['AB mag'] #, 'erg/s'] #, 'erg/s/Hz', 'erg/s/A']

def magabs2ergs(mag, fwhm, ZP=-48.57):
    r'''Transform absolute AB magnitude in luminosity.
    
    Parameters
    ----------
    
    mag : float
        magnitude in AB system
    fwhm : float
        FWHM of the filter in angstrom
    '''
    Fnu = 10**(-0.4*(mag-ZP))
    dist = 10*conversion['pc2ua']*conversion['ua2cm']
    Dnu = phys_const['c_cgs'] / (fwhm*conversion['aa2cm'])
    return 4*pi*dist**2*Fnu*Dnu

def magabs2fnu(mag, fwhm, ZP=-48.57):
    r'''Transform absolute AB magnitude in flux fnu (erg/s/Hz).
    
    Parameters
    ----------
    
    mag : float
        magnitude in AB system
    fwhm : float
        FWHM of the filter in angstrom
    '''
    Fnu = 10**(-0.4*(mag-ZP))
    Dnu = phys_const['c_cgs'] / (fwhm*conversion['aa2cm'])
    return Fnu*Dnu

def jy2cgs(flux):
    r'''Transform a flux in Jansky in :math:`erg \cdot s^{-1} \cdot cm^{-2} \cdot Hz^{-1}` (cgs system)

    .. math:: 
        1\ Jansky = 1\ Jy = 10^{-23}\ \frac{erg}{s \cdot cm^2 \cdot Hz}
       
    '''
    return conversion['jy2cgs']*flux

def cgs2jy(flux):
    r'''Transform a flux in Jansky in :math:`W \cdot m^{-2} \cdot Hz^{-1}` (SI system)

    .. math:: 
        1\ Jansky = 1\ Jy = 10^{26}\ \frac{W}{m^2 \cdot Hz}
    '''
    return flux/conversion['jy2cgs']

def lambda2nu(l):
    r'''Return the frequence nu in SI for a wavelength in angstrom
    TODO: Faire cela proprement avec les unites
    '''
    return  phys_const['c_si']/(l*conversion['aa2m'])

def nu2lambda(nu):
    r'''Return the wavelength nu in angstrom
    TODO: Faire cela proprement avec les unites
    '''
    return  (phys_const['c_si']*conversion['aa2m'])/nu

def fnu2flambda(wavelength, flux):
    r''' Transform :math:`f_{\nu}[erg/s/cm^{2}/Hz]` in :math:`f_{\lambda}[erg/s/cm^{2}/\AA]`
    
    .. math::
        \nu \times f_{\nu} = \lambda \times f_{\lambda}
    
    Parameters
    ----------
    
    wavelength : float
        the wavelength in \AA ( :math:`\lambda = \frac{c}{\nu})
    fnu : float
        flux in erg/s/cm^2/Hz
    
    Returns
    -------
    
    flux : float
        in :math:`erg/s/cm^2/\AA` 
    '''
    return (lambda2nu(wavelength)/wavelength)*flux

def mag2jy(M_AB, ZP=-48.57):
    r'''Transform a magnitude (AB system) in flux in Jansky [Jy]
    
     .. math:: 
        F[Jansky] &= 10^{23} F[erg \cdot s^{-1} \cdot cm^{-2} \cdot Hz^{-1}] \\
                  &= 10^{23} \cdot 10^{-0.4(M_{AB}-ZP)}
    
    with ZP=-48.57 or ZP=-48.6 for AB mag 
    
    Parameters
    ----------
    
    Returns
    -------
    
    '''
    return 10**(23-0.4*(M_AB-ZP))

def jy2mag(flux, ZP=-48.57):
    '''
    Transform a flux in Jansky [Jy] in magnitude (AB system)
     
    .. math:: 
        M_{AB} = 2.5*(23-log10(F[Jy]))+ZP
        
    with ZP=-48.57 or ZP=-48.6 for AB mag
    
    Parameters
    ----------
    
    Returns
    -------
    
    '''
    return 2.5*(23-log10(flux))+ZP

#TODO: do an iterator for all the formula

def mag2lum(mag):
    r'''function to transform absolute magnitude in luminosity.
    
    The method used is:
    
    .. math::
        L^{\star} = L_{\odot} \times 10^{0.4(M_{\odot}-M^\star)}
    
        
    with :math:`L_{\odot}` the solar bolometric luminosity and
    :math:`M_{\odot}` the solar bolometric magnitude.
    
    
    From Bessel et al 1998 [1]_:
    
    .. math::
        &L_{\odot} = 3.856 \times 10^{33} erg/s \\
        &M_{\odot} = 4.74
    
    References
    ----------
    
    [1] Bessel et al 1998 
    '''
    L_odot = 3.856e33
    M_odot = 4.74
    
    return L_odot*10**(0.4*(M_odot-mag)) 

def flux2luminosity(flux, dist=0):
    r'''Transform a flux in luminosity
    
    .. math::
        L = 4\pi \cdot D^2 \cdot F

    Parameters
    ----------
    
    flux : float
        The flux to transform in luminosity
    dist : float, optional
       distance of the source. If not given or nul the distance is 10 pc 
        
    '''
    if dist==0:
        dist = 10*conversion['pc2ua']*conversion['ua2cm']
    return 4*pi*dist**2*flux

def luminosity2flux(lum, dist=0):
    r'''Transform a luminosity in flux
    
    .. math::
        F = \frac{L}{4\pi \cdot D^2}
        
    Parameters
    ----------
    
    lum : float
        The luminosity to transform in flux
    dist : float, optional
       distance of the source. If not given or nul the distance is 10 pc 
     
    '''
    if dist==0:
        dist = 10*conversion['pc2ua']*conversion['ua2cm']
    return lum /(4*pi*dist**2)

def str2arcsec2(area):
    r'''Transform steradian in arcsecond.

    .. math::
        1 sr = \left(\frac{180*60*60}{\pi}\right)^2 \textit{arcsecond square}

    '''
    return area / (conversion['str2arcsec2'])


#class Unit(object):
#    #TODO:
#    def __init__(self):
#        pass