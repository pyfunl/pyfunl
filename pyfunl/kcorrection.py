#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (c) 2009 Nicolas Gruel All rights reserved.
# This program or module is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, 
# or (at your option) any later version. It is distributed in the hope that
# it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
# the GNU General Public License for more details.

import re

try:
    import numpy as np
    from scipy import interpolate
except:
    raise 'numpy or scipy not installed'

import kcorrectiondata

def kcorrec(z, catalog, filter, galaxytype):
    '''
    '''
    kcor = kcorrectiondata.read(catalog)
    redshift = kcor[str(filter)][:,0]
    index = kcor['columns'].index(str(galaxytype))
    corr = kcor[str(filter)][:,index]

    sp = interpolate.splrep(redshift,corr,s=0)
    return interpolate.splev(z,sp,der=0)