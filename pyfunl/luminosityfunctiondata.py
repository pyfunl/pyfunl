#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (c) 2009 Nicolas Gruel All rights reserved.
# This program or module is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, 
# or (at your option) any later version. It is distributed in the hope that
# it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
# the GNU General Public License for more details.

from __future__ import division

class LuminosityFunction(object):
    r'''A LuminosityFunction object hold the details of a luminosity function
    
    TODO: rajouter la LF a un certain redshift
    '''
    
    def __init__(self, name=None,
                 phistar=None, Lstar=None, Mstar=None, alpha=None, 
                 ephistar=None, eLstar=None, ealpha = None, redshift_lim=None,
                 filter_name=None, filter_fwhm=None,
                 unit=None, kcorr_filter=None, galaxytype=None, kcorr_cat=None, 
                 extinction=None):
        #redshift=None):
        ephistar = u'\u03b3\u03a6\u2736'
        eLstar = u'\u03b3L\u2736'
        ealpha = u'\u03b3\u03b1'

        self.header = ['Name', u'\u03a6\u2736', u'L\u2736', u'M\u2736', u'\u03b1', 
                       ephistar, eLstar, ealpha, 'z_lim',
                        'Unit', 'Filter', 'Galaxy Type','Kcorr','Av']
        self.name = name
        self.phistar = phistar
        self.Lstar = Lstar
        self.Mstar = Mstar
        self.alpha = alpha
        self.ephistar = ephistar
        self.eLstar = eLstar
        self.ealpha = ealpha
        self.redshift_lim = redshift_lim
        #TODO: To be implemented
#        self.redshift = redshift
        self.unit = unit
        self.filter_name = filter_name
        self.filter_fwhm = filter_fwhm
        self.galaxytype = galaxytype
        self.kcorr_cat = kcorr_cat
        self.kcorr_filter= kcorr_filter
        self.extinction = extinction

    def __str__(self):
        if not ((self.ephistar is None) or (self.eLstar is None) or 
                (self.ealpha is None) or (self.redshift_lim is None)):
            return ("%s %f %f %f %f %f %f %f %s %f %s %s %s %s %f\n" % 
                    (self.name, self.phistar, self.Lstar, self.Mstar, self.alpha,
                     self.ephistar, self.eLstar, self.ealpha, self.redshift_lim,
                     self.filter_name, self.filter_fwhm, self.unit, self.kcorr_cat, 
                     self.kcorr_filter, self.galaxytype,  self.extinction))
        else:
            return ("%s %f %f %f %s %f %s %s %s %f\n" % 
                    (self.name, self.phistar, self.Lstar, self.Mstar, self.alpha,
                     self.filter_name, self.filter_fwhm, self.unit, self.kcorr_cat, self.galaxytype, 
                     self.kcorr_filter, self.extinction))
            
    def tableItems(self):
        return (self.name,self.phistar, self.Lstar, self.Mstar, self.alpha,
                self.ephistar, self.eLstar, self.ealpha, self.redshift_lim,
                self.unit, self.filter_name, self.kcorr_cat, 
                self.galaxytype, self.kcorr_filter, self.extinction)

class LuminosityFunctionContainer(object):
    r'''LuminosityFunctionContainer holds a set of LuminosityFunction objects.

    TODO: Faire pour avoir liste de LF differentes...
    '''
    def __init__(self):
        pass