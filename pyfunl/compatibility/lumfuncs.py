from __future__ import division
from math import log10

import milia.lumfuncs

__docformat__ = "restructuredtext en"

class Schechter(milia.lumfuncs.Schechter):
    '''Compatibility class between milia.lumfincs library and pyfunl.lumfuncs.
    
    See Also
    --------
    
    lumfuncs
    
    Examples
    --------
    
    >>> from math import log10,log    
    >>> import milia.lumfuncs
    >>> 
    >>> h = 0.71 # cosmology
    >>> #B Band
    ... phi_star = 1.6e-2*h**3
    >>> M_star = -19.7+5*log10(h)
    >>> alpha = -1.07
    >>> #solar bolometrique luminosity and bolometric magnitude
    ... M_solar = 4.74
    >>> L_solar = 3.856e33 #erg/s
    >>> L_star = h**-2*10**(0.4*(M_solar-M_star)) * L_solar
    >>> 
    >>> lf_milia = milia.lumfuncs.Schechter(phi_star, L_star, alpha)      
    >>> lf_milia.function(L_star)
    2.3253959757508692e-47
    '''

    def __init__(self, *args ):
        if len(args) == 3:
            milia.lumfuncs.Schechter.__init__(self, args[0], args[1], args[2])
        elif len(args) == 7:
            milia.lumfuncs.Schechter.__init__(self, args[0], args[1], args[2],
                                              args[3], args[4], args[5], args[6])
        
    def number_objects(self, flrw, z1, z2, l1, l2):
        r'''Calcul the number of object between two different luminosity
        and a range of redshift by steradian.
        
        Parameters
        ----------
        
        flrw : class instance
           An instance of the class Flrw
                
        z1, z2 : float
            the redshift interval
                
        l1, l2 : float
            The luminosity cut-off
                
        Returns
        -------
        
        number_objects : float
            The number of object
            
        Examples
        --------
        >>> import compatibility.metrics as metrics                                                           
        >>> flrw = metrics.Flrw(71,0.27,0.73)                                                                
        >>> from math import log10,log
        >>> import compatibility.lumfuncs as lumfuncs
        >>>
        >>> h = 0.71 # cosmology
        >>> phi_star = 1.6e-2*h**3
        >>> M_star = -19.7+5*log10(h)
        >>> alpha = -1.07
        >>> M_solar = 4.74
        >>> L_solar = 3.856e33
        >>> L_star = h**-2*10**(0.4*(M_solar-M_star)) * L_solar
        >>> lf = lumfuncs.Schechter(phi_star,L_star,alpha)
        >>> lf.object_density(1e40,L_star)
        0.084361008761068557
        >>> lf.number_objects(flrw,0,1,1e40,L_star)
        1026636853.7917123
        >>> import astro_unit
        >>> astro_unit.str2arcsec2(lf.number_objects(flrw,0,1,1e40,L_star))
        0.024130514618825288
        '''
        #Verifie que Flrw est bien une instance de la class Flrw.


        try:
            vol1 = flrw.vol(z1)
            vol2 = flrw.vol(z2)
        except AttributeError:
            raise AttributeError, 'You need to provide a cosmology as a first parameter' 
        
        return (vol2-vol1)*self.object_density(l1,l2)