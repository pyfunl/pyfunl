from __future__ import division
from math import log10, pi

import milia.metrics

__docformat__ = "restructuredtext en"

class Flrw(milia.metrics.Flrw):
    r'''Class to have compatibility between milia library and coscalc.
    Some method as DM, get_hubble, set_hubble, get_matter, set_matter,
    get_vacuum, set_vacuum can or not be present. If not they are define 
    here. 
    
    Class to calcul the distance luminosity and some
    transformation between parsec and arcsec for different cosmology.
    It is based on David Hogg paper :
    
    http://xxx.unizar.es/abs/astro-ph/9905116
    
    result checked with with Ned Wright cosmology calculator
    
    http://www.astro.ucla.edu/~wright/CosmoCalc.html
    
    Flrw class constructor.
        
    Attributes
    ----------
    H0 : float
         :math:`H0` the hubble constant in km/s/Mpc (default 71 km/s/Mpc)
    OmegaM : float
             :math:`{\Omega}_M` the matter density (dimensionless) (default 0.27)
    OmegaL : float 
             :math:`{\Omega}_{\lambda}` the vacuum energy density (dimensionless) (default 0.73)

    Examples
    --------
    >>> import coscalc
    >>> print coscalc.Flrw(OmegaM=0.6).dl(z=0.1)
    450.669713172

    >>> import coscalc   
    >>> dislum = coscalc.Flrw(H0=50)
    >>> print dislum.DM(z=0.1)
    39.0507253513
    
    >>> import coscalc
    >>> dislum = coscalc.Flrw(70,0.3,0.7)
    >>> dislum.dl(z=1)
    6607.6575441767363

    '''
    def __init__(self, H0=71, OmegaM=0.27, OmegaL=0.73 ):
        milia.metrics.Flrw.__init__(self, H0, OmegaM, OmegaL )
    
#    def vol(self,z):
#        '''Function to calcul the volume for the global sphere.
#        
#        Milia function is giving the result by steradian.
#        '''
#        flrw = milia.metrics.Flrw(self.get_hubble(),self.get_matter(), self.get_vacuum())
#        return flrw.vol(z)*4*pi
    
    if not hasattr(milia.metrics.Flrw, 'DM'):
        def DM(self,z):
            '''Calcul the distance modulus for a certain redshift and cosmolgy (in mag) 
        
            Parameters
            ----------
            z : float
                redshift
            
            Returns
            -------
            DM : float
                 the distance modulus (mag)
            '''
            if z==0:
                return 0
            else:
                return 5*log10(self.dl(z))+25
            
    if not hasattr(milia.metrics.Flrw, 'get_hubble'):
        def get_hubble(self):
            '''Return the Hubble constant value used:
        
            Returns
            -------
            H0 : float
                :math:`H0` is the Hubble constant 
            '''
            return self.hubble

    if not hasattr(milia.metrics.Flrw,'set_hubble'):
        def set_hubble(self, H0):
            '''Set the Hubble constant :math:`H0` value and all the values derived directly from it
            (Hubble time t_h ) for the instance of Flrw. 
            
            Parameters
            ----------
            H0 : float
                 :math:`H0` is the Hubble constant 
    
            '''
            self.hubble = H0
    
    if not hasattr(milia.metrics.Flrw, 'get_matter'):
        def get_matter(self):
            '''Return the matter density :math:`{\Omega}_M` value used:
        
            Returns
            -------
            OmegaM : float
                     :math:`{\Omega}_M` is the matter density
            '''
            return self.matter
    
    if not hasattr(milia.metrics.Flrw,'set_matter'):
        def set_matter(self, OmegaM):
            '''Set the matter density :math:`{\Omega}_M` value for the instance of Flrw. 
        
            Parameters
            ----------
            OmegaM : float
                     :math:`{\Omega}_M` is the matter density
            '''
            self.matter = OmegaM

    if not hasattr(milia.metrics.Flrw, 'get_vacuum'):
        def get_vacuum(self):
            '''Return the vacuum density :math:`{\Omega}_{\lambda}` value used:
        
            Returns
            -------
            OmegaL : float
                     :math:`{\Omega}_{\lambda}` the vacuum density
            '''            
            return self.vacuum
        
    if not hasattr(milia.metrics.Flrw,'set_vacuum'):
        def set_vacuum(self, OmegaL):
            '''Set the vacuum density :math:`{\Omega}_{\lambda}` value for the instance of Flrw.
        
            Parameters
            ----------
            OmegaL : float
                     :math:`{\Omega}_{\lambda}` the vacuum density
                     
            '''
            self.vacuum = OmegaL