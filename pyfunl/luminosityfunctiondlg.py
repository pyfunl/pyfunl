#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (c) 2009 Nicolas Gruel All rights reserved.
# This program or module is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, 
# or (at your option) any later version. It is distributed in the hope that
# it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
# the GNU General Public License for more details.

import sys

from PyQt4.QtCore import *
from PyQt4.QtGui import *

import ui_luminosityfunctiondlg
import luminosityfunctiondata
import astro_unit
import kcorrectiondata
import filterdata

class LuminosityFunctionDlg(QDialog,
        ui_luminosityfunctiondlg.Ui_LuminosityFunctionDlg):
    '''
    '''
    def __init__(self,  parent=None):
        super(LuminosityFunctionDlg, self).__init__(parent)
        self.setupUi(self)
        
        self.LFdata = luminosityfunctiondata.LuminosityFunction()
        
        self.filters = filterdata.filters
        key = self.filters.keys()
        key.sort()
        self.filterComboBox.addItems(key)
        
        self.phistarLabel.setText(u'\u03a6<sup>\u2736</sup>')
        self.LstarLabel.setText(u'L<sup>\u2736</sup>')
        self.alphaLabel.setText(u'\u03b1')
        
        self.defaultForm()
        
        self.ephistarLabel.setText(u'\u03b3\u03a6')
        self.eLstarLabel.setText(u'\u03b3L')
        self.ealphaLabel.setText(u'\u03b3\u03b1')
        
        #equation... mettre une image peut etre
        ephistar = u'\u03a6<sup>\u2736</sup>(z)=\u03a6<sup>\u2736</sup><sub>0</sub>(1+z)<sup>\u03b3\u03a6</sup>'
        eLstar = u'L<sup>\u2736</sup>(z)=L<sup>\u2736</sup><sub>0</sub>(1+z)<sup>\u03b3L</sup>'
        ealpha = u'\u03b1(z)=\u03b1<sub>0</sub>+\u03b3<sub>\u03b1</sub>z'
        self.ephistarLabel_2.setText(ephistar)
        self.eLstarLabel_2.setText(eLstar)
        self.ealphaLabel_2.setText(ealpha)
        
        self.unitComboBox.addItems(astro_unit.LF_unit)
        
        self.frame.setEnabled(False)

#self.connect(self.metricsComboBox,SIGNAL("currentIndexChanged(int)"), self.updateComboBox)
#self.metricsComboBox = QComboBox()
#self.metricsComboBox.insertItem(0, '', [None,''])

        
        #Kcorrection:
        
        self.kcorrcataComboBox.insertItem(0,'')
        self.kcorrcataComboBox.addItems(kcorrectiondata.kcorrect_files.keys())
        self.connect(self.kcorrcataComboBox,SIGNAL("currentIndexChanged(int)"), self.updateComboBox)
        
        self.connect(self.buttonBox.button(QDialogButtonBox.Reset), SIGNAL('clicked()'), self.resetForm)        
        self.connect(self.buttonBox.button(QDialogButtonBox.Ok), SIGNAL('clicked()'), self.apply)
        self.connect(self.buttonBox.button(QDialogButtonBox.Cancel), SIGNAL('clicked()'), SLOT('close()'))

#TODO: implemente validation + return

    def updateComboBox(self):
        
        catalog = str(self.kcorrcataComboBox.currentText())
        if catalog=='':
            self.kcorrfilterComboBox.clear()
            self.galaxytypeComboBox.clear()
        else:
            kcorr = kcorrectiondata.read(catalog)
            filter = kcorr.keys()
            filter.remove('columns')
            filter.sort()
            self.kcorrfilterComboBox.addItems(filter)

            galaxytype = kcorr['columns'][1:]
            self.galaxytypeComboBox.addItems(galaxytype)    

    def validateName(self):
        '''Validate the name doesn't matter the name
        Perhaps can be used to replace some character but for now. 
        I don't care too much.
        '''
        pass
    
    def returnName(self):
        self.validateMatter()
        if self.LFnameLineEdit.text().isEmpty() or self.LFnameLineEdit.text() == ' ':
            self.LFnameLineEdit.setFocus()
        else:  
            self.vacuumEdit.setFocus()

    def validatePhi(self):
        pass
    
    def returnPhi(self):
        pass
    
    def validateLstar(self):
        pass
    
    def returnLstar(self):
        pass
    
    def validatealpha(self):
        pass
    
    def returnalpha(self):
        pass

    def validateePhi(self):
        pass
    
    def returnePhi(self):
        pass
    
    def validateeLstar(self):
        pass
    
    def returneLstar(self):
        pass
    
    def validateealpha(self):
        pass
    
    def returnealpha(self):
        pass

    def validatetype(self):
        pass
    
    def returntype(self):
        pass
    
    def validateextinction(self):
        pass
    
    def returnextinction(self):
        pass
        
    def validateFilter(self):
        pass
    
    def returnFilter(self):
        pass
    
    def validateUnit(self):
        pass
    
    def returnUnit(self):
        pass

    def defaultForm(self):
        r'''Fill the input as default
        '''
        self.LFnameLineedit.setText('Schechter LF (B)')
        self.phistarLineedit.setText('1.6e-2')
        self.LstarLineedit.setText('-19.7')
        self.alphaLineedit.setText('-1.07')
        #self.LFdata.unit = 'unit' #TODO:
        #self.LFdata.galaxytype = 'gal' #TODO:
        #self.LFdata.kcorr_cat = 'kcor' #TODO
        self.extinction_lineedit.setText('0')

    def resetForm(self):
        r'''Reset all the parameters'''
        self.LFnameLineedit.setText('')
    
        #TODO: reset filter combobox
        self.phistarLineedit.setText('')
        self.LstarLineedit.setText('')
        #TODO: reset unit combobox
        self.alphaLineedit.setText('')
    
        self.ephistarLineedit.setText('')
        self.eLstarLineedit.setText('')
        self.ealphaLineedit.setText('')
        self.redshift_limLineedit.setText('')

        self.kcorrcataComboBox.setCurrentIndex(0)
        #TODO: reset galaxy type and catalog combobox 
        self.extinction_lineedit.setText('')
        
    def apply(self):
        r'''
        '''
        self.LFdata.name = self.LFnameLineedit.text()
        self.LFdata.phistar = float(self.phistarLineedit.text())
        self.LFdata.Mstar = float(self.LstarLineedit.text())
        self.LFdata.alpha = float(self.alphaLineedit.text())
        try:
            self.LFdata.ephistar = float(self.ephistarLineedit.text())
            self.LFdata.eLstar = float(self.eLstarLineedit.text())
            self.LFdata.ealpha = float(self.ealphaLineedit.text())
            self.LFdata.redshift_lim = float(self.redshift_limLineedit.text())
        except:
            self.LFdata.ephistar = None
            self.LFdata.eLstar = None
            self.LFdata.ealpha = None
            self.LFdata.redshift_lim = None

        if self.kcorrcataComboBox.currentText() == '':
            self.LFdata.kcorr_cat = None
            self.LFdata.kcorr_filter = None        
            self.LFdata.galaxytype = None
        else:
            self.LFdata.kcorr_cat = self.kcorrcataComboBox.currentText()
            self.LFdata.kcorr_filter = self.kcorrfilterComboBox.currentText()        
            self.LFdata.galaxytype = self.galaxytypeComboBox.currentText()
            
        self.LFdata.filter_name = str(self.filterComboBox.currentText())
        
        if self.filters[self.LFdata.filter_name][2] == 'angstrom':
            self.LFdata.filter_fwhm = self.filters[self.LFdata.filter_name][1]
        elif  self.filters[self.LFdata.filter_name][2] == 'micrometer':
            self.LFdata.filter_fwhm = self.filters[self.LFdata.filter_name][1] * \
                                      astro_unit.conversion['mum2aa']        
                                      
        self.LFdata.Lstar = astro_unit.magabs2ergs(self.LFdata.Mstar,self.LFdata.filter_fwhm)
        self.LFdata.unit = self.unitComboBox.currentText()
        self.LFdata.extinction = float(self.extinction_lineedit.text())
        self.accept()

if __name__ == '__main__':
    app = QApplication(sys.argv)
    LFdialog = LuminosityFunctionDlg()
    LFdialog.show()
    app.exec_()
