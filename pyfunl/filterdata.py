#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (c) 2009 Nicolas Gruel All rights reserved.
# This program or module is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, 
# or (at your option) any later version. It is distributed in the hope that
# it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
# the GNU General Public License for more details.

from __future__ import division

import readcol

filter_data = readcol.readcol('/home/gruel/.pyfunl/data/filters.dat', comments='#')

filters = {}
for filter in filter_data:
    filters[filter[0]+' '+filter[1]] = [float(filter[2]),float(filter[3]),filter[4]] 
