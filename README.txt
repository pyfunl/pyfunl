This is pyfunl a software tool to predict the expected number of objects
belonging to a given population that would be detected by a well defined
 observational parameters.

It is using Milla https://guaix.fis.ucm.es/projects/milia a library create
to compute age and distance in a cosmological model.

This project will use PyQt as GUI library and if needed numpy, scipy and 
matplotlib as other tool.

# http://docs.scipy.org/doc/scipy/reference/constants.html
# http://calumgrant.net/units/units.html
# http://www.boost.org/doc/libs/1_37_0/doc/html/boost_units/Examples.html#boost_units.Examples.HeterogeneousUnitExample
# http://www.pyside.org/docs/pyside/howto-build/setup-apiextractor.html
# http://python.org/dev/peps/pep-0008/
# http://www.python.org/dev/peps/pep-0257/
# http://www.python.org/dev/peps/pep-0258/
# http://docutils.sourceforge.net/rst.html
# http://matplotlib.sourceforge.net/sampledoc/emacs_help.html
# http://sphinx.pocoo.org/ext/autodoc.html
# http://sphinx.pocoo.org/markup/desc.html
# http://projects.scipy.org/numpy/wiki/GitMirror
# http://docs.scipy.org/doc/numpy/reference/generated/numpy.cov.html#numpy.cov
# http://www.qtrac.eu/pyqtbook.html
# http://www.riverbankcomputing.co.uk/static/Docs/PyQt4/html/classes.html
# http://www.riverbankcomputing.co.uk/static/Docs/PyQt4/pyqt4ref.html#defining-new-signals-with-qtcore-pyqtsignal
# http://www.fileformat.info/info/unicode/char/03bb/index.htm

------------------------

Compile the ui part (not needed when the software will be provided)

pyuic4 -o ui_metrics.py metrics.ui
python setup.py install --prefix=/home/gruel/usr

pyuic4 -o ui_luminosityfunctiondlg.py luminosityfunctiondlg.ui 
