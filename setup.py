import os
from setuptools import setup, find_packages
version = '0.8'
README = os.path.join(os.path.dirname(__file__), 'README.txt')
long_description = open(README).read() + '\n\n'
setup(name='pyfunl',
      version=version,
      description=("A package that provide a GUI to calcul cosmological distances and ages, "
                    "from ISCAI"),
      long_description=long_description,
      classifiers=[
        "Programming Language :: Python",
        ],
      keywords='astronomy science',
      url='http://guaix.fis.ucm.es/projects/pyfunl/wiki',
      author='Nicolas Gruel',
      author_email='nicolas.gruel@gmail.com',
      license='GPLv3',
      packages=find_packages(exclude='tests'),

      packages_data = {'': ['*.dat', '*.txt', '*.rst']},

      #install_requires=['docutils>=0.5','distribute'],
      install_requires=['numpy', 'scipy'],
      entry_points = {
        'gui_scripts': [
            'coscalc = pyfunl.metrics:main',
            'pyfunl = pyfunl.pyfunl:main',
        ]
    }

     )
