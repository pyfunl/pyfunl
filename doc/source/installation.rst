============
Introduction
============

Prerequisites
-------------

Before to install **Pyfunl** you need to have a working `Python >= 2.5 <http:://www.python.org>`_
present on your system.
To be able to use the graphical interface (as a python software it's always possible to call it 
through a python instance) you also need to install the `library Qt <http:://qt.nokia.com>`_ and the 
python bindings `PyQt <http:://www.riverbankcomputing/co.uk>`_

**Pyfunl** has been created in the optics to be multi-plateforms and easy to install 
so the required softwares existe in a precompiled form for most of the plateforms 
(Unix, Linux, MacOSX, MS Windows).

Two others library are needed to execute some calcul: the numerical python library **numpy** and
the scientific python library **scipy**. Both should be installable, if not already present 
on the system, at pyfunl installation. 

An additional library for plotting can also be installed if desired 
`Matplotlib <http:://matplotlib.sourceforge.net>`_.

Installation
------------

To install **Pyfunl** you need to download the tarball `Pyfunl <XXXX>`_ 
or if you want to live on the edge the source code from the subversion repository::
 
	svn co https://guaix.fis.ucm.es/svn/trunk pyfunl

or the git repository::
 
	git clone git://gitorious.org/pyfunl/pyfunl.git

The installation for a global system can be done as:: 
	
	python setup.py install

or if you want to install it in a specific directory::

	python setup.py install --prefix=/your/preferred/directory

**Attention:** This directory should be include in your PYTHONPATH variable (on unix base system).

Example for linux with bash as shell and python version 2.5. Something like the following 
line should be added in the file ~/.bashrc::

	export PYTHONPATH=$PYTHONPATH:/your/preferred/directory/lib/python2.5/site-packages
	

