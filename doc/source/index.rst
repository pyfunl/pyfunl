.. pyfunl documentation master file, created by
   sphinx-quickstart on Tue Nov 24 10:50:54 2009.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pyfunl's documentation!
==================================

**Pyfunl** is a tool that calculate the number of objects belonging
to a given galaxy population as would be detected when observed
through a certain filter. It was created to become publicly available
and hopefully useful for any scientific team designing an

It implement some python modules to compute age and distance in a cosmological model
and also some Luminosity Function characterized by a Schechter parametrization 
(evolving or not).

These modules are API compatible with the library 
`Milia library <https://guaix.fis.ucm.es/projects/milia>`_ [#milia]_ 
which can be used to speed the calculation if available (an order of magnitude).

Contents:
=========

.. toctree::
   

   installation
   usage
   Modules/index
..   coscalc
   lumfuncs
   compatibility
   astro_unit

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. rubric:: Footnotes

.. [#milia] C++ Library wrote by Sergio Pascual, Universidad Complutense Madrid. 
